#ORM modules for TakatanJS.

Database layer modules to TakatanJS 3.0
Provides abstract layer for other modules to store and request data from different databases

under development! currently it has only has basic support of mongodb

## Bundle

Bundles describes database tables. It manages table fields.

## Field

Fields describes different types of data and manages field data

By default @takatanjs/orm has next fields:

 - any - holds any serialized with JSON data
 - boolean - holds yes/no data
 - datetime - holds date, time and timezone information
 - number - holds numbers
 - string - holds string data

Fields are designed to be extended with other modules, for example, check PasswordField at @takatanjs/auth module

## Collection

Collections provides access to search/update/remove data from database.
Each bundle automatically has according collection

## Node

Node represents single database entity. To append unique required functionality extends new class from Node class and set it for Collection as node class.

Check @takatanjs/auth User/Role modules for examples
