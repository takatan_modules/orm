const { Takatan, Log } = require('@takatanjs/core')
const Kefir = require('kefir')

const log = Log.m('ORM[Bundle]')

const Bundle = require('./bundle')
const AbstractCollection = require('./collection')
//const Node = require('../node/node')


class ORMBundleModule extends Takatan.extends('AbstractFactoryCoreModule')
{
  constructor(parent)
  {
    super(parent)
    this._bundles = {}

    this.factoryInit('bundle',Bundle)
  }

  moduleInit()
  {
    return this.M('db').defdb().loadBundles()
      .flatMap(list=>{
        log.log('compiling bundles',list)
        let $t = []
        list.forEach(b=>{
          let BCls = this.M('bundle').factoryClass('bundle',b.type)
          this._bundles[b.code] = new BCls(this,b)
          $t.push(this._bundles[b.code].compile()
            .map(coll=>{
              log.log('bundle compiled',coll.code())
              return coll
            }))
        })
        if (!$t.length)
          return Kefir.constant(true)
        return Kefir.concat($t).last()
      })
    /**/
  }

  bundle(code,createNew=false,createNewOfType='bundle')
  {
    if (this.bundleExists(code)&&!createNew) return this._bundles[code]
    if (!createNew) return null
    let Cls = this.factoryClass('bundle',createNewOfType)
    if (createNewOfType === 'bundle')
      Cls = this.M('db').defdb().bundleClass()
    //console.log(Cls)
    let ret = new Cls(this)
    ret.code(code)
    ret.onCompiled.onValue(bb=>{
      this._bundles[code] = bb
    })
    return ret
  }

  bundleExists(code)
  {
    if (this._bundles[code]) return true
    return false
  }

  bundleTable(code)
  {
    let b = this.bundle(code)
    if (!b) return null
    return b.dbTable()
  }

  collection(code)
  {
    if (!this.bundleExists(code)) return null
    return this.bundle(code).collection()
  }

  /**
  _newBundle(info)
  {
    return Rx.Observable.create(obs=>{
      //console.log(['new bundle',info])
      let b = new Bundle(this,info)
      b.prefix('u_')
      b.save().subscribe(bb=>{
        this._bundles[b.code()] = b
        this.obsEnd(obs,{success:true})
      },err=>{
        log.err('new bundle compile failed')
        console.log(err)
        this.obsEnd(obs,{success:false,error:'Bundle not compiled'})
      })
    })
  }

  _updateBundle(info)
  {
    return Rx.Observable.create(obs=>{
      log.info('updating bundle',info.code)
      let b = this.bundle(info.code)
      let regenFields = []
      info.fields.forEach(ff=>{
        if (!b.hasField(ff.code)&&ff.conf.genquantity&&(ff.generator!=''||typeof ff.def != 'undefined'))
        {
          regenFields.push(ff.code)
        }
      })
      b.name(info.name)
      b.desc(info.desc)
      b.fields(info.fields)

      b.save().subscribe(sb=>{
        if (!regenFields.length)
          return this.obsEnd(obs,{success:true})
        //let $t = regenFields.map(code=>b.collection().regenerateField(code))
        //console.log(regenFields)
        log.info('regenerating collection',info.code)
        b.collection().regenerateFields(regenFields).subscribe(rres=>{
          log.success('collection updated')
          this.obsEnd(obs,rres)
        },err=>{
          log.err('regenerating field failed')
          console.log(err)
          this.obsEnd(obs,{success:false,error:'Collection not updated'})
        })
      },err=>{
        log.err('bundle saving error!')
        console.log(err)
        this.obsEnd(obs,{success:false,error:'Bundle not saved!'})
      })
    })
  }

  _deleteBundle(info,dropData=false)
  {
    return Rx.of(this.bundle(info.code)).pipe(
      RxO.mergeMap(bndl=>{
        if (!bndl)
          return Rx.of(true)
        return bndl.drop()
      }),
      RxO.map(_=>{
        this._bundles[info.code] = null
        return true
      })
    )
  }

  /**/

}

module.exports = Takatan.register(ORMBundleModule)
