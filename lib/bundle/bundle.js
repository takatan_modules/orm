const { Takatan, Log } = require('@takatanjs/core')
const Kefir = require('kefir')

const log = Log.m('Bundle')

const AbstractFlaged = require('../db/flaged')
//const Collection = require('./collection')
//const Node = require('../node/node')

class Bundle extends Takatan.extends('AbstractCoreObject',AbstractFlaged)
{

  static info()
  {
    return {
      code: 'bundle',
      name: 'Bundle',
      description: 'Basic collection bundle (collection description manager)'
    }
  }

  constructor(parent,dbmodel=null)
  {
    super(parent)

    this._db = null
    this._new = true
    this._saved = false

    this._name = ''
    this._code = ''
    this._type = this.type()
    this._description = ''
    this._prefix = 'b_'
    this._fields = []
    this._relations = []

    this._schema = null
    this._collection = null
    //this._nodeClass = null

    if (dbmodel) this.fromDb(dbmodel)

    this.onCompiled = new Kefir.pool()
  }

  db(next)
  {
    if (typeof next !== 'undefined')
      this._db = next
    return this._db
  }

  collection()
  {
    return this._collection
  }

  type()
  {
    return this.meta().type
  }

  name(val)
  {
    if (typeof val == 'string')
    {
      this._name = val
      this._saved = false
    }
    return this._name
  }

  code(val)
  {
    if (typeof val == 'string')
    {
      this._code = val
      this._saved = false
    }
    return this._code
  }

  prefix(val)
  {
    if (typeof val == 'string')
    {
      this._prefix = val
      this._saved = false
    }
    return this._prefix
  }

  description(val)
  {
    if (typeof val == 'string')
    {
      this._description = val
      this._saved = false
    }
    return this._description
  }

  fields(val)
  {
    if (Array.isArray(val))
    {
      this._fields = val
      this._saved = false
    }
    return this._fields
  }

  getField(code)
  {
    return this._fields.reduce((acc,el)=>{
      return el.code==code?el:acc
    },null)
  }

  hasField(code)
  {
    return this._fields.reduce((acc,el)=>{
      return el.code==code?true:acc
    },false)
  }

  addField(field)
  {
    if (this.hasField(field.code)) return
    this._fields.push(field)
    this._saved = false
  }

  deleteField(code)
  {
    if (!this.hasField(code)) return
    this._fields = this._fields.filter(f=>f.code!=code)
    this._saved = false
  }

  updateField(field)
  {
    if (!this.hasField(field.code)) return
    for(let i=0;i<this._fields.length;i++)
      if (this._fields[i].code == field.code)
        this._fields[i] = field
    this._saved = false
  }

  toObject() { return this.toDb() }
  /**/
  toDb()
  {
    return {
      type: this.type(),
      name: this._name,
      code: this._code,
      prefix: this._prefix,
      description: this._description,
      flags: this._flags,
      fields: this._fields,
      relations: this._relations
    }
  }

  fromDb(db)
  {
    //console.log(db)
    let ddb =  db
    if (typeof db.toObject == 'function')
      ddb = db.toObject()
    this._name = ddb.name
    this._code = ddb.code
    this._prefix = ddb.prefix
    this._description = ddb.description
    this._relations = ddb.relations
    this._flags = ddb.flags
    this._fields = ddb.fields

    this._new = false
    this._saved = true
  }

  save(recompile=true,dropData=false)
  {
    return Kefir.constant(this._saved)
      .flatMap(saved=>{
        if (saved)
          return Kefir.constant(this)
        log.log('Saving bundle',this.code())
        return this.M('db').defdb().saveBundle(this.code(),this.toDb())
      })
      .flatMap(self=>{
        log.success('bundle saved',this.code())
        if (!recompile)
          return Kefir.constant(this)
        return this.compile(dropData)
      })
  /**
    return Rx.of(this._saved).pipe(
      RxO.mergeMap(saved=>{
        if (saved)
          return Rx.of(this)
        log.log('Saving bundle',this.code())
        return this.M('db').defdb().saveBundle(this.code(),this.toDb())
      }),
      RxO.mergeMap(self=>{
        log.success('bundle saved',this.code())
        if (!recompile)
          return Rx.of(this)
        return this.compile(dropData).pipe(
          RxO.tap(_=>log.success('recompiled',this.code()))
        )
      })
    )
  /**/
  }

  compile(dropData=false)
  {
    return Kefir.constantError({code:'ormerror.bundle.compile_should_be_reimplemented'})
  }

  drop()
  {
    return Kefir.constantError({code:'ormerror.bundle.drop_should_be_reimplemented'})
  /**
    return this.M('db').dropBundleTable(this.dbTable(),true).pipe(
      RxO.mergeMap(_=>{
        return Rx.from(MgBundle.findOneAndRemove({code:this._code}))
      })
    )
  /**/
  }

  dbTable()
  {
    return this.prefix()+this.code()
  }

  setNodeClass(Cls)
  {
    //if (!this._collection) return
    //this._collection.setNodeClass(Cls)
  }

  /*
  _fieldHasFlag(field,flag)
  {
    return field.flags.reduce((acc,el)=>{
      if (el==flag) return true
      return acc
    },false)
  }
  */

}

module.exports = Takatan.register(Bundle)
