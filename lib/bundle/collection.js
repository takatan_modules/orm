const { Takatan, Log } = require('@takatanjs/core')
const Kefir = require('kefir')

const log = Log.m('Collection')

const Node = require('../node/node')

class AbstractCollection extends Takatan.extends('AbstractCoreObject')
{

  constructor(prnt,schema)
  {
    super(prnt)
    //this._core = this._parent._core
    this._bundle = prnt
    this._schema = schema
    this._nodeClass = Node
    this._classFn = null
    this._model = null
    this._initModel()
  }

  _initModel()
  {
  }

  code()
  {
    return this._bundle.code()
  }

  bundle()
  {
    return this._bundle
  }

  model()
  {
    return this._model
  }

  setNodeClass(Cls)
  {
    this._nodeClass = Cls
  }

  setNodeClassFn(fn)
  {
    if (typeof fn == 'function')
      this._classFn = fn
  }

  nodeClass(mdl,opts=null)
  {
    let Cls = Node
    if (this._nodeClass)
      Cls = this._nodeClass
    if (this._classFn&&typeof this._classFn == 'function')
    {
      let Clsfn = this._classFn(mdl)
      if (Cls.isPrototypeOf(Clsfn))
        Cls = Clsfn
    }
    if (opts&&opts.cls&&Cls.isPrototypeOf(opts.cls))
    {
      Cls = opts.cls
    } else {
      if (opts&&typeof(opts.clsfn)=='function')
      {
        let CCls = opts.clsfn(el)
        if (Cls.isPrototypeOf(CCls))
          Cls = CCls
      }
    }
    if (!Cls)
      Cls = Node
    return Cls
  }

  saveNode(mdl)
  {
    return Kefir.constantError({code:'error.abstract_collection.saveNode.should_be_reimplemented'})
  }

  loadNode(mdl,opts=null)
  {
    return Kefir.constantError({code:'error.abstract_collection.loadNode.should_be_reimplemented'})
  }

  newNode(defs=null)
  {
    return Kefir.constantError({code:'error.abstract_collection.newNode.should_be_reimplemented'})
  }

  initNode(ctx=null)
  {
    return Kefir.constantError({code:'error.abstract_collection.initNode.should_be_reimplemented'})
  }

  findId(id)
  {
    return Kefir.constantError({code:'error.abstract_collection.findId.should_be_reimplemented'})
  }

  findNode(query,opts)
  {
    return Kefir.constantError({code:'error.abstract_collection.findNode.should_be_reimplemented'})
  }

  findNodes(query,opts)
  {
    return Kefir.constantError({code:'error.abstract_collection.findNodes.should_be_reimplemented'})
  }

  updateNodes(query,data,params)
  {
    return Kefir.constantError({code:'error.abstract_collection.updateNodes.should_be_reimplemented'})
  }

  countNodes(query)
  {
    return Kefir.constantError({code:'error.abstract_collection.countNodes.should_be_reimplemented'})
  }

  removeNodes(query)
  {
    return Kefir.constantError({code:'error.abstract_collection.removeNodes.should_be_reimplemented'})
  }

  regenerateFields(codes)
  {
    return Kefir.constantError({code:'error.abstract_collection.regenerateFields.should_be_reimplemented'})
  }

}

module.exports = Takatan.register(AbstractCollection)
