const { Takatan, Log } = require('@takatanjs/core')

const log = Log.m('ORM[ORM]')

class ORMOrmModule extends Takatan.extends('AbstractFactoryCoreModule')
{
  /*
  this is just "proxy" module for correct dependency tree
  */
}

module.exports = Takatan.register(ORMOrmModule)
