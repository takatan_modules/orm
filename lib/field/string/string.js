const { Takatan } = require('@takatanjs/core')

const AbstractField = require('../field')

class StringField extends Takatan.extends(AbstractField)
{

  static schema(info,sch={}) {
    sch.type = String
    return sch
  }

  static info()
  {
    return {
      code: 'string',
      name: 'Text/string',
      config: {
        ops: [{
          name: 'Format',
          type: 'string',
          manual: false,
          description: 'Text format',
          defaultValue: 'small',
          list: [
            {label:'Single line',value:'small'},
            {label:'Big text',value:'big'}
          ]
        }]
      }
    }
  }

  /*
  xtypes()
  {
    let isBigConf = this.confOp(0,false)
    let type = this.constructor.info().code
    let ret = []
    if (isBigConf)
      ret.push(type+':big')
    else
      ret.push(type+':small')
    return ret
  }
  */

  _v2db(v) {
    if(typeof v == 'undefined'||v===null) return null
    if(typeof v != 'string') return v.toString()
    return v
  }
  _db2v(db) {
    if(typeof(db)=='undefinded'||db===null) return null
    return db
  }

  /*
  _serializev(val)
  {
    return val+''
  }

  _unserializev(val)
  {
    return val+''
  }
  */


}

module.exports = Takatan.register(StringField)
