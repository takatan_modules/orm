const { Takatan, Log } = require('@takatanjs/core')
const Kefir = require('kefir')

const log = Log.m('StringFieldModule')

//const CoreModule = require('../../core/abstract/module')
const StringField = require('./string')

//const UUIDGenerator = require('./generators/uuid')
//const HashGenerator = require('./generators/hash')

//const RegexpTest = require('./tests/regexp')
//const ContainsTest = require('./tests/contains')
//const EqTest = require('./tests/eq')
//const InListTest = require('./tests/inlist')
//const ListIncludeTest = require('./tests/listinclude')

//const EqSetter = require('./setters/eq')
//const ListFromStrSetter = require('./setters/listfromstr')
//const Push2ArrSetter = require('./setters/push2arr')
//const RemoveFromArraySetter = require('./setters/remfromarr')

//const AnyAsString = require('./modifiers/anyasstring')
//const StringLength = require('./modifiers/length')

class StringFieldModule extends Takatan.extends('AbstractCoreModule')
{
  moduleRegister()
  {
    return Kefir.stream(obs=>{
      this.M('field').factoryRegisterClass('field_type',StringField)
      this.obsEnd(obs)
    })

    //this._core.module('field').registerGenerator(UUIDGenerator)
    //this._core.module('field').registerGenerator(HashGenerator)

    //this._core.module('field').registerSetter(EqSetter)
    //this._core.module('field').registerSetter(ListFromStrSetter)
    //this._core.module('field').registerSetter(Push2ArrSetter)
    //this._core.module('field').registerSetter(RemoveFromArraySetter)

    //this._core.module('field').registerModifier(AnyAsString)
    //this._core.module('field').registerModifier(StringLength)

    //this._core.module('logic').registerLogicTest(RegexpTest)
    //this._core.module('logic').registerLogicTest(EqTest)
    //this._core.module('logic').registerLogicTest(ContainsTest)
    //this._core.module('logic').registerLogicTest(InListTest)
    //this._core.module('logic').registerLogicTest(ListIncludeTest)
  }

}

module.exports = Takatan.register(StringFieldModule)
