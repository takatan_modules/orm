let Rx = require('rxjs')
let crypto = require('crypto')

let log = require('../../../../log').m('HashGenerator')
let g18n = require('../../../../g18n')

const AbstractGenerator = require('../../generator')

class HashGenerator extends AbstractGenerator
{

  static info()
  {
    return {
      code: 'hash',
      name: g18n.t('Hash generator'),
      types: ['string'],
      ops: [{
        type: 'number',
        name: g18n.t('Length'),
        value: 8,
        manual: true
      }]
    }
  }

  generate()
  {
    return Rx.Observable.create(obs=>{
      let len = 16
      //if (this._conf&&this._conf.len)
        //len = parseInt(this._conf.len)
      if (this._conf&&this._conf[0])
        len = parseInt(this._conf[0])||len
      if (this._conf&&this._conf.ops)
      {
        len = parseInt(this._conf.ops[0].target)||len
        //console.log(['!!!',this._conf.ops])
      }
      //console.log(['hash generator',len,this._conf])
      let val = crypto.randomBytes(len).toString('hex').slice(0,len)
      obs.next(val)
      obs.complete()
    })
  }

}

module.exports = HashGenerator

