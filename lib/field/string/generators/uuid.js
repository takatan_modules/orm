let Rx = require('rxjs')
let uuid = require('uuid-v4')

let log = require('../../../../log').m('UUIDGenerator')
let g18n = require('../../../../g18n')

const AbstractGenerator = require('../../generator')

class UUIDGenerator extends AbstractGenerator
{

  static info()
  {
    return {
      code: 'uuid',
      name: g18n.t('UUID generator'),
      types: ['string'],
      ops: []
    }
  }

  generate()
  {
    return Rx.Observable.create(obs=>{
      let val = uuid()
      this.obsEnd(obs,val)
    })
  }

}

module.exports = UUIDGenerator

