const { Takatan, Log } = require('@takatanjs/core')
const Kefir = require('kefir')

const log = Log.m('DateTimeFieldModule')

const TimeZoneField = require('./timezone')
const DatetimeField = require('./datetime')
//const NowGenerator = require('./generators/now')
//const SysTZGenerator = require('./generators/systz')
////const ShiftGenerator = require('./generators/shift')
//const EqTest = require('./tests/eq')
//const OlderTest = require('./tests/older')
//const YangerTest = require('./tests/yanger')

//const TzFromStringSetter = require('./setters/tzfromstring')
//const DTLFromStringSetter = require('./setters/dtlfromstring')

class DatetimeFieldModule extends Takatan.extends('AbstractCoreModule')
{
  moduleRegister()
  {
    return Kefir.stream(obs=>{
      this.M('field').factoryRegisterClass('field_type',TimeZoneField)
      this.M('field').factoryRegisterClass('field_type',DatetimeField)
      //this.M('field').registerGenerator(NowGenerator)
      //this.M('field').registerGenerator(SysTZGenerator)
      ////this.M('field').registerGenerator(ShiftGenerator)

      //this.M('logic').registerLogicTest(EqTest)
      //this.M('logic').registerLogicTest(OlderTest)
      //this.M('logic').registerLogicTest(YangerTest)

      //this.M('field').registerSetter(TzFromStringSetter)
      //this.M('field').registerSetter(DTLFromStringSetter)
      this.obsEnd(obs)
    })
  }

  granFormat(conf=[])
  {
    let ret = ''
    if (conf.includes('year'))
    {
      if (ret!='') ret+='-'
      ret += 'YYYY'
    }
    if (conf.includes('month'))
    {
      if (ret!='') ret+='-'
      ret += 'MM'
    }
    if (conf.includes('day'))
    {
      if (ret!='') ret+='-'
      ret += 'DD'
    }
    if (conf.includes('hour'))
    {
      if (ret!='') ret+='T'
      ret += 'HH'
    }
    if (conf.includes('minute'))
    {
      if (ret!='') ret+=':'
      ret += 'mm'
    }
    if (conf.includes('second'))
    {
      if (ret!='') ret+=':'
      ret += 'ss'
    }
    if (ret=='')
      ret = 'YYYY-MM-DDTHH:mm:ss'
    //ret += 'ZZ'
    return ret
  }


}

module.exports = Takatan.register(DatetimeFieldModule)
