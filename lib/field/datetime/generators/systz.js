let Rx = require('rxjs')
//let Mtz = require('moment-timezone')

let log = require('../../../../log').m('SysTZGenerator')
let g18n = require('../../../../g18n')

const AbstractGenerator = require('../../generator')

class SysTZGenerator extends AbstractGenerator
{

  static info()
  {
    return {
      code: 'systz',
      name: g18n.t('System Time Zone'),
      types: ['timezone'],
      ops: []
    }
  }

  generate()
  {
    return Rx.Observable.create(obs=>{
      let val = this._core.timezone
      this.obsEnd(obs,val)
    })
  }

}

module.exports = SysTZGenerator

