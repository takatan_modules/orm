let Rx = require('rxjs')
let Mtz = require('moment-timezone')

let log = require('../../../../log').m('NowGenerator')
let g18n = require('../../../../g18n')

const AbstractGenerator = require('../../generator')

class NowGenerator extends AbstractGenerator
{

  static info()
  {
    return {
      code: 'now',
      name: g18n.t('Now'),
      types: ['datetime'],
      ops: []
    }
  }

  generate(ctx=null)
  {
    return Rx.Observable.create(obs=>{
      let val = Mtz.tz(this._core.timezone)
      //console.log(val)
      obs.next(val)
      obs.complete()
    })
  }

  generateSync(ctx=null)
  {
    let val = Mtz().tz(this._core.timezone)
    return val.format('YYYY-MM-DDTHH:mm:ssZZ')
  }

}

module.exports = NowGenerator

