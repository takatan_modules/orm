let Rx = require('rxjs')
let Mtz = require('moment-timezone')

let log = require('../../../../log').m('ShiftGenerator')
let g18n = require('../../../../g18n')

const AbstractGenerator = require('../../generator')

class ShiftGenerator extends AbstractGenerator
{

  static info()
  {
    return {
      code: 'dtshift',
      name: g18n.t('Date/time shift'),
      types: ['datetime'],
      ops: [{
        name: g18n.t('Base'),
        type: 'datetime',
        manual: true
      },{
        name: g18n.t('Shift'),
        type: 'string',
        manual: true
      }]
    }
  }

  generate()
  {
    return Rx.Observable.create(obs=>{
      let val = Mtz.tz(this._core.timezone)
      //console.log(val)
      obs.next(val)
      obs.complete()
    })
  }

}

module.exports = ShiftGenerator

