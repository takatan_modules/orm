const { Takatan } = require('@takatanjs/core')
const Mtz = require('moment-timezone')

//let log = require('../../../log').m('TimeZoneField')
//let g18n = require('../../../g18n')

const AbstractField = require('../field')

class TimeZoneField extends Takatan.extends(AbstractField)
{

  static schema(info,schema={})
  {
    schema.type = String
    return schema
  }

  static info()
  {
    let zones = Mtz.tz.names()
    //console.log(zones)
    return {
      code: 'timezone',
      name: 'Time zone',
      config: {
        list: zones
      }
    }
  }

  _v2db(v) {
    return v
    /*
    if (!v)
      return null
    let tz = this._core.timezone
    if (this.conf&&this.conf.timezone)
      tz = this.conf.timezone
    let m = null
    if (typeof v == 'number')
      m = Mtz.unix(v).tz(tz)
    else
      m = Mtz(v).tz(tz)
    if (m.isValid()) return m.format()
    return null
    */
  }

  _db2v(db) {
    return db
    /*
    let tz = this._core.timezone
    if (this.conf&&this.conf.timezone)
      tz = this.conf.timezone
    let m = Mtz(db).tz(tz)
    if (m.isValid()) return m
    return null
    */
  }

}

module.exports = Takatan.register(TimeZoneField)
