const { Takatan } = require('@takatanjs/core')
const Mtz = require('moment-timezone')

const AbstractField = require('../field')

class DatetimeField extends Takatan.extends(AbstractField)
{

  static schema(info,schema={})
  {
    schema.type = Date
    return schema
  }

  static info()
  {
    return {
      code: 'datetime',
      name: 'Date and time',
      config: {
        ops: [{
          type: 'timezone',
          name: 'Time Zone',
          manual: true,
          canNone: false
        },{
          type: '[string]',
          name: 'Granularity',
          manual: false,
          list: [
            {label:'Second',value:'second'},
            {label:'Minute',value:'minute'},
            {label:'Hour',value:'hour'},
            {label:'Day',value:'day'},
            {label:'Month',value:'month'},
            {label:'Year',value:'year'},
          ],
          canNone: false
        }]
      }
    }
  }

  _format()
  {
    let conf = this.configOp(1,[])
    //console.log(conf)
    let ret = ''
    if (conf.includes('year'))
    {
      if (ret!='') ret+='-'
      ret += 'YYYY'
    }
    if (conf.includes('month'))
    {
      if (ret!='') ret+='-'
      ret += 'MM'
    }
    if (conf.includes('day'))
    {
      if (ret!='') ret+='-'
      ret += 'DD'
    }
    if (conf.includes('hour'))
    {
      if (ret!='') ret+='T'
      ret += 'HH'
    }
    if (conf.includes('minute'))
    {
      if (ret!='') ret+=':'
      ret += 'mm'
    }
    if (conf.includes('second'))
    {
      if (ret!='') ret+=':'
      ret += 'ss'
    }
    if (ret=='')
      ret = 'YYYY-MM-DDTHH:mm:ss'
    ret += 'ZZ'
    return ret
  }

  _tz()
  {
    let ret = this.configOp(0,null)
    //console.log(['dt.tz',ret])
    if (!ret)
      ret = Mtz.tz.guess()
      //ret = this.M('config').conf('core.timezone')
    return ret
  }

  _v2db(v,i)
  {
    if (!v)
      return null
    let tz = this._tz()
    let f = this._format()
    let m = null
    if (typeof v == 'number')
      m = Mtz.unix(v).tz(tz)
    else
      m = Mtz(v,f).tz(tz)
    m = this._defGran(m)
    let dbf = 'YYYY-MM-DDTHH:mm:ssZZ'
    //console.log(['_v2db',this.code,v,m.format(dbf)])
    if (m&&typeof(m.isValid)=='function'&&m.isValid())
    {
      this._parent._populated[i] = m
      return m.utc().toDate()
    }
    return null
  }

  _db2v(db,i)
  {
    let tz = this._tz()
    let f = this._format()
    let dbf = 'YYYY-MM-DDTHH:mm:ssZZ'
    //let mmt = Mtz(db,dbf)
    let m = this._defGran(Mtz(db,dbf).tz(tz))
    //console.log(['datetime._db2v',this.code,tz,db,m.format(f)])
    if (m&&typeof(m.isValid)=='function'&&m.isValid())
    {
      this._parent._populated[i] = m
      return m.format(f)
    }
    return null
  }

  /*
  _serializev(v)
  {
    if (!v)
      return null
    let tz = this._tz()
    let f = this._format()
    let m = null
    if (typeof v == 'number')
      m = Mtz.unix(v).tz(tz)
    else
      m = Mtz(v,f).tz(tz)
    m = this._defGran(m)
    let dbf = 'YYYY-MM-DDTHH:mm:ssZZ'
    //console.log(['_v2db',this.code,v,m.format(dbf)])
    if (m&&typeof(m.isValid)=='function'&&m.isValid())
    {
      return m.utc().toString()
    }
    return null
  }

  _unserializev(v)
  {
    let tz = this._tz()
    let f = this._format()
    let dbf = 'YYYY-MM-DDTHH:mm:ssZZ'
    //let mmt = Mtz(db,dbf)
    let m = this._defGran(Mtz(db,dbf).tz(tz))
    //console.log(['datetime._db2v',this.code,tz,db,m.format(f)])
    if (m&&typeof(m.isValid)=='function'&&m.isValid())
    {
      return m.format(f)
    }
    return null
  }
  */


  now()
  {
    let tz = this._tz()
    let m = this._defGran(Mtz().tz(tz))
    return m
  }

  _defGran(mmt)
  {
    if (!mmt||typeof(mmt.isValid)!='function'||!mmt.isValid())
      return mmt
    let conf = this.configOp(1,[])
    if (!conf.length)
      conf = ['second','minute','hour','day','month','year']
    if (!conf.includes('year'))
      mmt.year(2001)
    if (!conf.includes('month'))
      mmt.month(1)
    if (!conf.includes('day'))
      mmt.date(1)
    if (!conf.includes('hour'))
      mmt.hour(0)
    if (!conf.includes('minute'))
      mmt.minute(0)
    if (!conf.includes('second'))
      mmt.seconds(0)
    //console.log(['saveDT',mmt])
    return mmt
  }

}

module.exports = Takatan.register(DatetimeField)
