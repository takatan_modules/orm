const { Takatan, Log } = require('@takatanjs/core')
const Kefir = require('kefir')

const log = Log.m('NumberFieldModule')

const NumberField = require('./number')

//const RandomGenerator = require('./generators/random')

//const EqSetter = require('./setters/eq')
//const IncSetter = require('./setters/inc')
//const DecSetter = require('./setters/dec')
//const MulSetter = require('./setters/mul')
//const DivSetter = require('./setters/div')

//const EqTest = require('./tests/eq')
//const LtTest = require('./tests/lt')
//const GtTest = require('./tests/gt')

//const NumberAsBoolModifier = require('./modifiers/numberasboolean')
//const AnyAsNumber = require('./modifiers/anyasnumber')

class NumberFieldModule extends Takatan.extends('AbstractCoreModule')
{

  moduleRegister()
  {
    return Kefir.stream(obs=>{
      this.M('field').factoryRegisterClass('field_type',NumberField)
      this.obsEnd(obs)
    })
  }


  /*<S-F9>
  constructor(parent)
  {
    super(parent)
    this._core.module('field').registerFieldType(NumberField)
    this._core.module('field').registerGenerator(RandomGenerator)
    this._core.module('field').registerSetter(EqSetter)
    this._core.module('field').registerSetter(IncSetter)
    this._core.module('field').registerSetter(DecSetter)
    this._core.module('field').registerSetter(MulSetter)
    this._core.module('field').registerSetter(DivSetter)
    this._core.module('field').registerModifier(NumberAsBoolModifier)
    this._core.module('field').registerModifier(AnyAsNumber)

    this._core.module('logic').registerLogicTest(EqTest)
    this._core.module('logic').registerLogicTest(LtTest)
    this._core.module('logic').registerLogicTest(GtTest)
  }
  */

}

module.exports = Takatan.register(NumberFieldModule)
