let Rx = require('rxjs')
let crypto = require('crypto')

let log = require('../../../../log').m('RandomGenerator')
let g18n = require('../../../../g18n')

const AbstractGenerator = require('../../generator')

class RandomGenerator extends AbstractGenerator
{

  static info()
  {
    return {
      code: 'random',
      name: g18n.t('Random number generator'),
      types: ['number'],
      ops: [{
        type: 'number',
        name: g18n.t('Min'),
        value: 0,
        manual: true
      },{
        type: 'number',
        name: g18n.t('Max'),
        value: 100,
        manual: true
      }]
    }
  }

  generate()
  {
    return Rx.Observable.create(obs=>{
      let min = 0
      let max = 100
      if (this._conf&&this._conf[0])
        min = parseInt(this._conf[0])
      if (this._conf&&this._conf[1])
        max = parseInt(this._conf[1])
      let delta = max-min
      let val = Math.floor(Math.random()*(delta+1))+min
      obs.next(val)
      obs.complete()
    })
  }

}

module.exports = RandomGenerator

