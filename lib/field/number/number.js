const { Takatan } = require('@takatanjs/core')

const AbstractField = require('../field')

class NumberField extends Takatan.extends(AbstractField)
{

  static schema(info,schema={})
  {
    schema.type = Number
    return schema
  }

  static info()
  {
    return {
      code: 'number',
      name: 'Number',
      config: {
        ops: [{
          name: 'Is Real',
          desc: 'by default all numbers are Integers',
          type: 'boolean',
          manual: true
        },{
          name: 'Rational digits',
          type: 'number',
          manual: true,
          def: 2
        }]
      }
    }
  }

  /*
  xtypes()
  {
    let isFloat = this.confOp(0,false)
    let type = this.constructor.info().code
    let ret = []
    if (isFloat)
      ret.push(type+':float')
    else
      ret.push(type+':integer')
    return ret
  }
  */

  _v2db(v) {
    if(typeof v == 'undefinded'||v===null) return null
    if(typeof parseFloat(v) != 'number') return null
    return parseFloat(v)
  }
  _db2v(db) {
    if(typeof(db)=='undefinded'||db===null) return null
    return db
  }

}

module.exports = Takatan.register(NumberField)
