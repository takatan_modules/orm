const { Takatan, Log } = require('@takatanjs/core')
const Kefir = require('kefir')

const log = Log.m('AbstractFieldValidator')

class AbstractFieldValidator extends Takatan.extends('AbstractCoreObject')
{

  constructor(parent,conf=null)
  {
    super(parent)
    /*
    this._field = this._parent
    this._node = this._field._parent
    //this._core = this._field._core
    this._conf = conf
    if (!this._conf)
      this._conf = {
        invert:false
      }
    /**/
  }

  static info()
  {
    return {
      code: 'nocare',
      name: 'Allways ok',
      iname: 'Allways fail',
      types: ['*','[]','[*]'],
      ops: []
    }
  }

  /*
  validate()
  {
    return Rx.Observable.create(obs=>{
      obs.next({valid:!this._conf.invert,error:'nocare not valid!'})
      obs.complete()
    })
  }
  */

}

module.exports = Takatan.register(AbstractFieldValidator)

