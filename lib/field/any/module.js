const { Takatan, Log } = require('@takatanjs/core')
const Kefir = require('kefir')

const log = Log.m('AnyFieldModule')

//let log = require('../../../log').m('AnyFieldModule')

//const CoreModule = require('../../core/abstract/module')
const AnyField = require('./any')

//const EmptyValidator = require('./validators/empty')
//const IsNullValidator = require('./validators/isnull')
//const RequiredValidator = require('./validators/required')
//const UniqueValidator = require('./validators/unique')

//const IsNullTest = require('./tests/isnull')
//const ExistsTest = require('./tests/exists')

//const AllModifier = require('./modifiers/all')
//const AnyOneModifier = require('./modifiers/anyone')
//const AtPosModifier = require('./modifiers/atpos')
//const SizeModifier = require('./modifiers/size')

//const AtPathModifier = require('./modifiers/atpath')

//const AnyAsString = require('./modifiers/anyasstring')

//const ClearSetter = require('./setters/clear')
//const DoNothingSetter = require('./setters/donothing')

class AnyFieldModule extends Takatan.extends('AbstractCoreModule')
{
  moduleRegister()
  {
    return Kefir.stream(obs=>{
      this.M('field').factoryRegisterClass('field_type',AnyField)
      this.obsEnd(obs)
    })

    //this._core.module('field').registerValidator(EmptyValidator)
    //this._core.module('field').registerValidator(IsNullValidator)
    //this._core.module('field').registerValidator(RequiredValidator)
    //this._core.module('field').registerValidator(UniqueValidator)

    //this._core.module('field').registerSetter(ClearSetter)
    //this._core.module('field').registerSetter(DoNothingSetter)

    //this._core.module('logic').registerLogicTest(IsNullTest)
    //this._core.module('logic').registerLogicTest(ExistsTest)

    //this._core.module('field').registerModifier(AllModifier)
    //this._core.module('field').registerModifier(AnyOneModifier)
    //this._core.module('field').registerModifier(AtPosModifier)
    //this._core.module('field').registerModifier(SizeModifier)
    //this._core.module('field').registerModifier(AtPathModifier)

    //this._core.module('field').registerModifier(AnyAsString)
    //this._core.module('logic').registerLogicTest(NotOneTest)
  }

}

module.exports = Takatan.register(AnyFieldModule)
