const { Takatan } = require('@takatanjs/core')

const AbstractField = require('../field')

class AnyField extends Takatan.extends(AbstractField)
{

  static info()
  {
    return {
      code: 'any',
      name: 'Any data'
    }
  }

}

module.exports = Takatan.register(AnyField)
