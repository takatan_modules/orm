const { Takatan, Log, Kefir } = require('@takatanjs/core')

const log = Log.m('AbstractField')

const Flaged = require('../db/flaged')

const mg = require('mongoose')

class AbstractField extends Takatan.extends('AbstractCoreObject',Flaged)
{

  constructor(parent,info)
  {
    super(parent)
    this._parent = parent

    this._info = info

    this._name = info.name
    this._code = info.code
    this._type = info.type
    this._quantity = info.quantity
    this._generator = info.generator
    this._defaultValue = info.defaultValue
    this._config = info.config
    this._flags = info.flags

    /*
    this._validators = []

    //if (info.code=='created')
    //{
      //console.log(['field constructor',this._parent._model,this.config])
    //}
    this.flags.forEach(flag=>{
      let validator = this.M('field').genValidator(this,flag)
      if (validator&&validator.code!='nocare')
        this.addValidator(validator)
    })
    */
  }

  static info()
  {
    return {
      code: 'abstract',
      name: 'Abstract field'
      /*
      config: {
        generator: [ops]
        ops:[{
          type, name....
        }]
      }
      */
    }
  }

  static schema(info,schema = {},db='mongodb')
  {
    if (db==='mongodb')
    {
      schema.type = mg.Schema.Types.Mixed
      return schema
    }
  }

  static populateSchema(finfo) { return null }

  name() { return this._name }
  code() { return this._code }
  description() { return this._description }
  type() { return this._type }
  quantity() { return this._quantity }
  defaultValue() { return this._defaultValue }
  config() { return this._config }

  validate()
  {
    return Kefir.constant({valid:true})
  }

  init()
  {
    //by default field do nothing on init
  }

  _v2db(v,i) {
    if (this.hasFlag('virtual'))
      return v
    return JSON.stringify(v)
  }

  _db2v(db,i) {
    if (this.hasFlag('virtual'))
      return db
    if(typeof db == 'undefined') return
    return JSON.parse(db)
  }

  size()
  {
    return this._parent._model[this._code].length
  }

  has(val)
  {
    let ret = false
    this.forEach(v=>{
      if (ret) return
      if (v instanceof Takatan.class('Node')&&v.eq(val))
      {
        ret = true
        return
      }
      if (val===ret)
        ret = true
    })
    return ret
  }

  populate(depth=0)
  {
    return Kefir.constant(this)
  }

  all(vals)
  {
    if (typeof(vals)!='undefined'&&Array.isArray(vals))
    {
      this.clear()
      vals.forEach((v,i)=>{
        this.set(v,i)
      })
    }
    if (!Array.isArray(this._parent._model[this._code]))
      return []
    return this._parent._model[this._code].map(d=>{
      return this._db2v(d)
    })
  }

  forEach(fn)
  {
    if (typeof fn != 'function')
      return this
    for(let i=0;i<this.size();i++)
      fn(this.getp(i),i)
    return this
  }

  filter(fn,self=true)
  {
    if (typeof fn != 'function')
      return this
    let next = []
    for(let i=0;i<this.size();i++)
      if(fn(this.getp(i),i))
        next.push(this.getp(i))
    if (!self)
      return next
    this.all(next)
    return this
  }

  reduce(fn,init)
  {
    let ret = init
    if (typeof fn != 'function')
      return ret
    for(let i=0;i<this.size();i++)
      ret = fn(ret,this.getp(i),i)
    return ret
  }

  get(index=0)
  {
    if (index<0) index = 0
    if (index>0&&this._quantity&&index>=this._quantity)
      index = this._quantity-1
    let ret
    if (this.hasFlag('virtual'))
    {
      ret = this._parent._model[this._code][index]
      return ret
    }
    if (!this._parent._model||!Array.isArray(this._parent._model[this._code]))
      return ret
    return this._db2v(this._parent._model[this._code][index],index)
  }

  getp(index=0)
  {
    if (index<0) index = 0
    if (index>0&&this._quantity&&index>=this._quantity)
      index = this._quantity-1
    if (typeof(this._parent._populated[this._code][index])!='undefined')
      return this._parent._populated[this._code][index]
    return this.get(index)
  }


  set(v,index=0)
  {
    if (index<0) index = 0
    if (index>0&&this._quantity&&index>=this._quantity)
      index = this._quantity-1
    this._saved = false
    if (!Array.isArray( this._parent._model[this._code]))
      this._parent._model[this._code] = []
    if (!Array.isArray( this._parent._populated[this._code]))
      this._parent._model[this._code] = []
    if (this.hasFlag('virtual'))
    {
      this._parent._model[this._code][index] = v
      this._parent._populated[this._code][index] = v
    } else {
      this._parent._model[this._code][index] = this._v2db(v,index)
    }
    //this._parent._model.markModified(this.code)
    return
  }

  push(v,flags=null)
  {
    if (!flags)
      return this.set(v,this.size())
    if (flags.unique)
    {
      let dbv = this._v2db(v)
      if (!this._parent._model[this._code].reduce((acc,vv)=>{
        if(dbv===null||vv===null)
          return dbv===vv?true:acc
        if(typeof(dbv.toString)=='function'&&typeof(vv.toString)=='function')
          return dbv.toString()==vv.toString()?true:acc
        return dbv==vv?true:acc
      },false))
        this.set(v,this.size())
    }
  }

  //generation mechanism can be reimplemented
  genDefaultAt(i,ctx=null)
  {
    return Kefir.constant(this._defaultValue)
  }

  genDefault(ctx=null)
  {
    return Kefir.stream(obs=>{
      log.log('generating default value',this._code)
      let $t = []
      let amount = 0
      if (this._config&&parseInt(this._config.genquantity))
        amount = parseInt(this._config.genquantity)
      if (this._quantity<amount&&this._quantity>1)
        amount = this.quantity
      if (this._quantity==1)
        amount = 1

      log.log('generate default values:',{code:this._code,amount})
      for (let i=0;i<amount;i++)
        $t.push(this.getDefaultAt(i,ctx))
      let gi = 0
      Kefir.concat($t).observe(v=>{
        //console.log(['generated value',v])
        if (typeof v != 'undefined')
          this.set(v,gi)
        gi++
      },err=>{
        log.error('field generating default failed',{code:this._code,generator:this.generator,i:gi})
        console.log(err)
        this.obsErr(obs,err)
      },()=>{
         this.obsEnd(obs,this) 
      })
    })
  }

  clear()
  {
    this._parent._model[this._code] = []
    this._parent._populated[this._code] = []
    //this._parent._model.markModified(this.code)
  }

  clearAt(i=0)
  {
    if(Array.isArray(this._parent._model[this._code]))
      this._parent._model[this._code] = this._parent._model[this._code].filter((x,ii)=>i!=ii)
    if(Array.isArray(this._parent._model[this._code]))
      this._parent._populated[this._code] = this._parent._populated[this._code].filter((x,ii)=>i!=ii)
    //this._parent._model.markModified(this.code)
  }

  configOp(opi,def)
  {
    let ret = def
    if(this._config&&this._config.ops&&this._config.ops[opi])
    {
      let op = this._config.ops[opi]
      if (op.source=='manual')
        ret = op.manual
      else
        ret = this._parent.f(op.source) // TODO
    }
    return ret
  }

}

module.exports = Takatan.register(AbstractField)
