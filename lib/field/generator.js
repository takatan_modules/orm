const { Takatan, Log, Kefir } = require('@takatanjs/core')

const log = Log.m('AbstractFieldGenerator')

class AbstractFieldGenerator extends Takatan.extends('AbstractCoreObject')
{

  static info()
  {
    return {
      code: 'null',
      name: 'Null generator',
      types: ['*'],
      ops: []
    }
  }

  generate()
  {
    return Kefir.constant(null)
  }

  confOp(opi,def)
  {
    let ret = def
    if(Array.isArray(this._config)&&typeof this._config[opi] != 'undefined')
      ret = this._config[opi]
    return ret
  }

}

module.exports = Takatan.register(AbstractFieldGenerator)
