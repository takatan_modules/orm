const { Takatan, Log } = require('@takatanjs/core')
const Kefir = require('kefir')

const log = Log.m('ORM[Field]')

const AbstractField = require('./field')
//const AbstractFieldValidator = require('./validator')
//const AbstractFieldGenerator = require('./generator')
//const AbstractWidget = require('./widget')

class ORMFieldModule extends Takatan.extends('AbstractFactoryCoreModule')
{
  constructor(parent)
  {
    super(parent)

    this.factoryInit('field_type',AbstractField)
  }

  fieldSchema(info,schema={},dbtype)
  {
    let Cls = this.factoryClass('field_type',info.type)
    return Cls.schema(info,schema,dbtype)
  }


  /*
  registerFieldType(Field)
  {
    let info = Field.info()
    let ex = this._allTypes.reduce((acc,x)=>x.info().code==info.code?true:acc,false)
    if (ex) return
    log.log('registring field type',info.code)
    this._allTypes.push(Field)
    //this._fields[info.code] = Field
  }

  fieldTypes()
  {
    let ret = []
    //for(let code in this._fields)
      //ret.push(this._fields[code].info())
    return this._allTypes.map(x=>x.info())
  }

  registerValidator(Validator)
  {
    let info = Validator.info()
    log.log('regisrting validator type',info.code)
    this._allValidators.push(Validator)
  }

  registerGenerator(Generator)
  {
    let info = Generator.info()
    log.log('regisrting field generator',info.code)
    this._allGenerators.push(Generator)
  }

  registerSetter(Setter)
  {
    let info = Setter.info()
    log.log('regisrting field setter',info.code)
    this._allSetters.push(Setter)
  }

  registerModifier(Modifier)
  {
    let info = Modifier.info()
    log.log('regisrting field modifier',info.code)
    this._allModifiers.push(Modifier)
  }

  registerWidget(Widget)
  {
    let info = Widget.info()
    log.log('regisrting field widget',info.code)
    this._allWidgets.push(Widget)
  }
  */
  fieldPopulateSchema(info)
  {
    let Cls = this.factoryClass('field_type',info.type)
    return Cls.populateSchema(info)
  }
/*
  classField(code,def='any')
  {
    let Cls = this._allTypes.reduce((acc,x)=>{
      //console.log(x.info())
      return x.info().code==code?x:acc
    },null)
    if (!Cls)
    {
      log.warn('field type not found!',code)
      Cls = this._allTypes.reduce((acc,x)=>{
        return x.info().code==def?x:acc
      },null)
    }
    return Cls
  }
  */

  genField(node,info,ctx=null)
  {
    let Cls = this.factoryClass('field_type',info.type)
    let ret = new Cls(node,info,ctx)
    return ret
  }

  /*
  classValidator(code,def='nocare')
  {
    let Cls = this._allValidators.reduce((acc,x)=>{
      return x.info().code==code?x:acc
    },null)
    if (!Cls)
      Cls = this._allValidators.reduce((acc,x)=>{
        return x.info().code==def?x:acc
      },null)
    return Cls
  }

  genValidator(field,code,conf=null)
  {
    let Cls = this.classValidator(code)
    let ret = new Cls(field,conf)
    return ret
  }

  classGenerator(code,def='null')
  {
    let Cls = this._allGenerators.reduce((acc,x)=>{
      return x.info().code==code?x:acc
    },null)
    if (!Cls)
      Cls = this._allGenerators.reduce((acc,x)=>{
        return x.info().code==def?x:acc
      },null)
    return Cls
  }

  genGenerator(field,code,conf=null)
  {
    let Cls = this.classGenerator(code)
    let ret = new Cls(field,conf)
    return ret
  }

  getGeneratorClass(code)
  {
    return this.classGenerator(code)
  }

  classSetter(dataType,code)
  {
    let Cls = this._allSetters.reduce((acc,x)=>{
      return x.info().code==code&&this._dtok(dataType,x.info())?x:acc
    },null)
    return Cls
  }

  classModifier(dataType,code)
  {
    let Cls = this._allModifiers.reduce((acc,x)=>{
      return x.info().code==code&&this._dtok(dataType,x.info())?x:acc
    },null)
    return Cls
  }

  _dtok(dt,inf)
  {
    let isArr = dt.startsWith('[')&&dt.endsWith(']')
    if (inf.types.includes(dt))
      return true
    if (isArr)
    {
      if (inf.types.includes('[]'))
        return true
      if (inf.types.includes('[*]'))
        return true
    } else {
      if (inf.types.includes('*'))
        return true
    }
    return false
  }

  dashboardRoutes(app)
  {
    return Rx.Observable.create(obs=>{

      let rtFieldTypes = {
        url: '/api/fields/types',
        access: 'auth.dashboard',
        method: 'get',
        fn: (ctx)=>{
          return Rx.Observable.create(obs=>{
            let lst = this._allTypes.map(x=>{
              let ret = x.info()
              ret.xtype = 'type'
              return ret
            })
            this.obsEnd(obs,lst)
          })
        }
      }

      let rtFieldGenerators = {
        url: '/api/fields/generators',
        access: 'auth.dashboard',
        method: 'get',
        fn: (ctx)=>{
          return Rx.Observable.create(obs=>{
            let lst = this._allGenerators.map(x=>{
              let ret = x.info()
              ret.xtype = 'generator'
              return ret
            })
            this.obsEnd(obs,lst)
          })
        }
      }

      let rtFieldValidators = {
        url: '/api/fields/validators',
        access: 'auth.dashboard',
        method: 'get',
        fn: (ctx)=>{
          return Rx.Observable.create(obs=>{
            let lst = this._allValidators.map(x=>{
              let ret = x.info()
              ret.xtype = 'validator'
              return ret
            })
            this.obsEnd(obs,lst)
          })
        }
      }

      let rtFieldSetters = {
        url: '/api/fields/setters',
        access: 'auth.dashboard',
        method: 'get',
        fn: (ctx)=>{
          return Rx.Observable.create(obs=>{
            let lst = this._allSetters.map(x=>{
              let ret = x.info()
              ret.xtype = 'setter'
              return ret
            })
            this.obsEnd(obs,lst)
          })
        }
      }


      let rtFieldModifiers = {
        url: '/api/fields/modifiers',
        access: 'auth.dashboard',
        method: 'get',
        fn: (ctx)=>{
          return Rx.Observable.create(obs=>{
            let lst = this._allModifiers.map(x=>{
              let ret = x.info()
              ret.xtype = 'modifier'
              return ret
            })
            this.obsEnd(obs,lst)
          })
        }
      }


      let rtFieldClasses = {
        url: '/api/fields/classes',
        access: 'auth.dashboard',
        method: 'get',
        fn: (ctx)=>{
          return Rx.Observable.create(obs=>{
            let mods = this._allModifiers.map(x=>{
              let ret = x.info()
              ret.xtype = 'modifier'
              return ret
            })
            let setts = this._allSetters.map(x=>{
              let ret = x.info()
              ret.xtype = 'setter'
              return ret
            })
            let vlds = this._allValidators.map(x=>{
              let ret = x.info()
              ret.xtype = 'validator'
              return ret
            })
            let gens = this._allGenerators.map(x=>{
              let ret = x.info()
              ret.xtype = 'generator'
              return ret
            })
            let tps = this._allTypes.map(x=>{
              let ret = x.info()
              ret.xtype = 'type'
              return ret
            })
            let wgts = this._allWidgets.map(x=>{
              let ret = x.info()
              ret.xtype = 'widget'
              return ret
            })
            this.obsEnd(obs,{
              types: tps,
              setters: setts,
              modifiers: mods,
              validators: vlds,
              generators: gens,
              widgets: wgts
            })
          })
        }
      }



      this.obsEnd(obs,[
        rtFieldTypes,
        rtFieldGenerators,
        rtFieldSetters,
        rtFieldModifiers,
        rtFieldValidators,
        rtFieldClasses
      ])
    })



  }
  */
}

module.exports = Takatan.register(ORMFieldModule)
