let Rx = require('rxjs')
let Mtz = require('moment-timezone')

let log = require('../../../../log').m('RandomBoolGenerator')
let g18n = require('../../../../g18n')

const AbstractGenerator = require('../../generator')

class RandomBoolGenerator extends AbstractGenerator
{

  static info()
  {
    return {
      code: 'randombool',
      name: g18n.t('Random'),
      types: ['boolean'],
      ops: [{
        type: 'number',
        name: g18n.t('Success chance %'),
        def: 50,
        manual: true
      }]
    }
  }

  generate()
  {
    return Rx.Observable.create(obs=>{
      let chance = parseFloat(this.confOp(0)) || 50
      let rnd = Math.random()*100
      let val = false
      if (rnd<=chance)
        val = true
      this.obsEnd(obs,val)
    })
  }

}

module.exports = RandomBoolGenerator

