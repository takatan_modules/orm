const { Takatan } = require('@takatanjs/core')

const AbstractField = require('../field')

class BooleanField extends Takatan.extends(AbstractField)
{
  static schema(info,schema={}) {
    schema.type = Boolean
    return schema
  }

  static info()
  {
    return {
      code: 'boolean',
      name: 'Yes/No',
      config: {
        ops: [{
          type: 'string',
          manual: true,
          name: 'On label',
          def: 'Yes'
        },{
          type: 'string',
          manual: true,
          name: 'Off label',
          def: 'No'
        }]
      }
    }
  }

  _v2db(v) {
    if(typeof v == 'undefinded'||v===null) return null
    if(typeof v != 'boolean') return !!v
    return v
  }
  _db2v(db) {
    if(typeof(db)=='undefinded'||db===null) return null
    return db
  }

}

module.exports = Takatan.register(BooleanField)
