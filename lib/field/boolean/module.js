const { Takatan, Log } = require('@takatanjs/core')
const Kefir = require('kefir')

const log = Log.m('BooleanFieldModule')

const BooleanField = require('./boolean')
//const RandomBoolGenerator = require('./generators/random')
//const EqTest = require('./tests/eq')
//const EqSetter = require('./setters/eq')

class BooleanFieldModule extends Takatan.extends('AbstractCoreModule')
{

  /**
  constructor(parent)
  {
    super(parent)
    this.M('field').registerFieldType(BooleanField)
    this.M('field').registerGenerator(RandomBoolGenerator)
    this.M('field').registerSetter(EqSetter)
    this.M('logic').registerLogicTest(EqTest)
  }
  /**/

  moduleRegister()
  {
    return Kefir.stream(obs=>{
      this.M('field').factoryRegisterClass('field_type',BooleanField)
      this.obsEnd(obs)
    })
  }


}

module.exports = Takatan.register(BooleanFieldModule)
