/*
  This file is part of TakatanJS software.
  @author abrakadobr
  @version 2.5.57
  @website https://js.takatan.tk
*/


let Rx = require('rxjs')

let log = require('../../../../log').m('StringFieldModifier<any>')
let g18n = require('../../../../g18n')

const AbstractModifier = require('../../modifier')

const Node = require('../../../node/node')

class StringFieldModifier extends AbstractModifier
{

  static info()
  {
    return {
      code: 'stringfield',
      name: g18n.t('String field'), //getter
      iname: g18n.t('String field'), //setter
      invert: false, //cuz not invertable
      flat: true, //modifier value is single element
      strict: 'string', //keeps original field type
      ops: [{
        type: 'string',
        name: g18n.t('field code'),
        manual: true
      }],
      types: ['relation']
    }
  }

  getNext()
  {
    let node = this._dataTarget.one()
    let path = this.op()
    let val
    if (typeof node =='undefined')
      return val
    if (!(node instanceof Node))
    {
      if (typeof node[path] != 'undefined')
        val = node[path]+''
      return val
    }
    let f = node.field(path)
    if (!f)
      return val
    val = node.f(path)+''
    return val
  }



}

module.exports = StringFieldModifier
