/*
  This file is part of TakatanJS software.
  @author abrakadobr
  @version 2.5.57
  @website https://js.takatan.tk
*/


let Rx = require('rxjs')

let log = require('../../../../log').m('NumberFieldModifier<any>')
let g18n = require('../../../../g18n')

const AbstractModifier = require('../../modifier')

const Node = require('../../../node/node')

class NumberFieldModifier extends AbstractModifier
{

  static info()
  {
    return {
      code: 'numberfield',
      name: g18n.t('Number field'), //getter
      iname: g18n.t('Number field'), //setter
      invert: false, //cuz not invertable
      flat: true, //modifier value is single element
      strict: 'number', //keeps original field type
      ops: [{
        type: 'string',
        name: g18n.t('field code'),
        manual: true
      }],
      types: ['relation']
    }
  }

  modifyTarget(tgt)
  {
    let node = this._dataTarget.one()
    let path = this.op()
    //console.log(node,path)
    if (typeof node =='undefined'||!node)
      return tgt
    if (!(node instanceof Node))
      return tgt
    let f = node.field(path)
    if (!f)
      return tgt
    return f
  }

  getNext()
  {
    let node = this._dataTarget.one()
    let path = this.op()
    let val
    if (typeof node =='undefined')
      return val
    if (!(node instanceof Node))
    {
      if (typeof node[path] != 'undefined')
        val = parseFloat(node[path])
      return val
    }
    let f = node.field(path)
    if (!f)
      return val
    val = parseFloat(node.f(path))
    return val
  }

}

module.exports = NumberFieldModifier
