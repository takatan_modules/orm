/*
  This file is part of TakatanJS software.
  @author abrakadobr
  @version 2.5.57
  @website https://js.takatan.tk
*/


let Rx = require('rxjs')

let log = require('../../../../log').m('RelationFieldModifier<any>')
let g18n = require('../../../../g18n')

const AbstractModifier = require('../../modifier')

const Node = require('../../../node/node')

class RelationFieldModifier extends AbstractModifier
{

  static info()
  {
    return {
      code: 'relationfield',
      name: g18n.t('Relation field'), //getter
      iname: g18n.t('Relation field'), //setter
      invert: false, //cuz not invertable
      flat: true, //modifier value is single element
      strict: 'relation', //keeps original field type
      ops: [{
        type: 'string',
        name: g18n.t('field code'),
        manual: true
      },{
        type: 'boolean',
        name: '[]',
        manual: true
      }],
      types: ['relation']
    }
  }

  getNext()
  {
    let node = this._dataTarget.one()
    let path = this.op()
    let isArr = this.op(1,false)
    let val
    if (typeof node =='undefined')
      return val
    if (!(node instanceof Node))
    {
      if (typeof node[path] != 'undefined')
        val = node[path]
      return val
    }
    let f = node.field(path)
    if (!f)
      return val
    if (isArr)
      val = node.field(path).all().map((x,xi)=>node.field(path).getp(xi))
    else
      val = node.fp(path)
    return val
  }



}

module.exports = RelationFieldModifier
