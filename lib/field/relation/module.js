const { Takatan, Log, Kefir } = require('@takatanjs/core')

const log = Log.m('RelationFieldModule')

const RelationField = require('./relation')

//const NumberFieldModifier = require('./modifiers/numfield')
//const StringFieldModifier = require('./modifiers/strfield')
//const BooleanFieldModifier = require('./modifiers/boolfield')
//const DatetimeFieldModifier = require('./modifiers/dtfield')
//const RelationFieldModifier = require('./modifiers/relfield')

//const EqSetter = require('./setters/eq')

//const EqTest = require('./tests/eq')

class RelationFieldModule extends Takatan.extends('AbstractCoreModule')
{
  moduleRegister()
  {
    return Kefir.stream(obs=>{
      this.M('field').factoryRegisterClass('field_type',RelationField)
      this.obsEnd(obs)
    })

    //this.M('field').registerFieldType(RelationField)
    //this.M('field').registerSetter(EqSetter)
    //this.M('field').registerModifier(NumberFieldModifier)
    //this.M('field').registerModifier(StringFieldModifier)
    //this.M('field').registerModifier(BooleanFieldModifier)
    //this.M('field').registerModifier(DatetimeFieldModifier)
    //this.M('field').registerModifier(RelationFieldModifier)

    //this.M('logic').registerLogicTest(EqTest)
  }

}

module.exports = Takatan.register(RelationFieldModule)
