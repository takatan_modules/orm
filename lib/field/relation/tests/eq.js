/*
  This file is part of TakatanJS software.
  @author abrakadobr
  @version 2.5.57
  @website https://js.takatan.tk
*/


let Rx = require('rxjs')

let log = require('../../../../log').m('EqTest')
let g18n = require('../../../../g18n')

const AbstractTest = require('../../../logic/abstract/test')
const Node = require('../../../node/node')

class EqTest extends AbstractTest
{

  static info()
  {
    return {
      code: 'eq',
      invert: true,
      name: g18n.t('Equal'),
      iname: g18n.t('Not equal'),
      ops: [{
        type: 'relation',
        name: g18n.t('to'),
        manual: true
      }],
      types: ['relation']
    }
  }

  _test(v)
  {
    let v2 = this.op()
    if (v&&typeof v.eq == 'function')
      return v.eq(v2)
    return false
  }

  queryFor(ctx,vctx)
  {
    return Rx.Observable.create(obs=>{
      let op0 = this.op()
      if (!op0)
        return this.obsEnd(obs,null)
      let tgt = this.dataTarget().targetCode()
      let id = null
      if (op0 instanceof Node)
      {
        id = op0.id()
      } else {
        if (op0.id)
          id = op0.id
      }
      if (!id)
        return this.obsEnd(obs,null)
      let ret = {}
      ret[tgt] = id
      if (this._conf.invert)
        ret[tgt] = {$not:id}
      //console.log(ret)
      return this.obsEnd(obs,ret)
    })
  }





}

module.exports = EqTest
