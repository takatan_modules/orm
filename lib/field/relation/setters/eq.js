/*
  This file is part of TakatanJS software.
  @author abrakadobr
  @version 2.5.57
  @website https://js.takatan.tk
*/


let Rx = require('rxjs')

let log = require('../../../../log').m('EqSetter<relation>')
let g18n = require('../../../../g18n')

const AbstractSetter = require('../../../logic/setter')

class EqSetter extends AbstractSetter
{

  static info()
  {
    return {
      code: 'eq',
      invert: false,
      name: g18n.t('Set'),
      iname: '',
      ops: [{
        name: 'to',
        manual: true,
        type: 'relation'
      }],
      types: ['relation']
    }
  }

  setRange(range,target)
  {
    if (!range.length)
      return false  //can't work with arrays
    let val = this.op()
    //log.warn('set',[range,val])
    range.forEach(pos=>{
      target.setModified(val,pos)
    })
    return true
  }

}

module.exports = EqSetter
