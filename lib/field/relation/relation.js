const { Takatan, Log, Kefir } = require('@takatanjs/core')
const mg = require('mongoose')
const ObjectId = mg.Schema.Types.ObjectId
const log = Log.m('RelationField')

const AbstractField = require('../field')

class RelationField extends Takatan.extends(AbstractField)
{

  static schema(info,schema={})
  {
    schema.type = ObjectId
    let ref = RelationField.populateSchema(info)
    if (ref)
      schema.ref = ref
    //console.log(['relSchema',schema])
    //log.err('relation schema',[info.code,ref,schema])
    return schema
  }

  static populateSchema(info)
  {
    if (!info||!info.config||!info.config.ops[0])
      return null
    let targets = info.config.ops[0].manual
    if (targets=='')
      targets = info.conf.ops[0].source
    //log.warn('populateSchema',[info.code,targets])
    if (Array.isArray(targets)&&targets.length)
      return targets[0]
    if (typeof targets == 'string'&&targets.length)
      return  targets
    return null
  }

  static info()
  {
    return {
      code: 'relation',
      name: 'Relation',
      config: {
        ops:[{
          name: 'Target',
          type: 'bundle',
          manual: false,
          canNone: false
        }]
      }
    }
  }

  init()
  {
    //console.log(['INIT RELATION FIELD',this.code,this._parent._model[this._code]])
    if(!Array.isArray(this._parent._model[this._code]))
      return
    //console.log([this.conf,this._conf])
    //let tb = this.confOp(0,null)
    if (!this._config||!this._config.ops||!this._config.ops[0])
      return
    let op = this._config.ops[0]
    let tb = op.manual
    if (!tb||!tb.length)
      tb = op.source
    //console.log(tb)
    this._targetBundle = ''
    if (!tb)
      return
    this._targetBundle = tb
    //console.log('init pop from',this._parent._model[this._code])
    this._parent._model[this._code].forEach((x,xi)=>{
      //console.log('init population',x)
      if (!x||!x._id) return
      let xid = x+''
      if (xid==x._id) return
      //if (typeof x == 'object')
      //{
        //console.log(x,xid,x+''==xid,xi,'INIT RELATION FIELD is objectid?')
      //}
      //if (typeof x != 'object') return
      //if (!x._id) return
      let nd = this.C(tb).loadNode(x)
      //console.log(x,typeof x,xi,'INIT RELATION FIELD is Node',[nd])
      this._parent._populated[this._code][xi] = nd
    })
  }

  _rv2db(v,i,Cls)
  {
    if (typeof v == 'undefined')
      return null
    if (v===null)
      return v
    if (v instanceof ObjectId)
      return v
    if (v instanceof Cls)
    {
      if(!Array.isArray(this._parent._populated[this._code]))
        this._parent._populated[this._code] = []
      this._parent._populated[this._code][i] = v
      return v.id()
    }
    let oid = new ObjectId(v,{suppressWarning:true})
    if (oid.path&&(oid.path==v||oid.path==v.toString()))
      return v
    if (oid.toString() == v)
      return v
    if (v.toString&&oid.toString()==v.toString())
      return v
    return null
  }

  _v2db(v,i) {
    return this._rv2db(v,i,Takatan.class('Node'))
  }

  _db2v(db,i) {
    if (!db) return db
    if (db._id)
      return db._id
    return db
  }

  populateFromCollection(code,depth=0)
  {
    return this.C(code).findNodes({_id:{$in:this._parent._model[this._code]}},{populate:depth})
    .map(list=>{
      //console.log(['POP COLL',this.code,code,list.length])
      this._parent._populated[this._code] = []
      this.all().forEach((sid,si)=>{
        if (!sid) return
        let s = list.reduce((acc,val)=>{
          //console.log([val.id(),sid,val.id().toString()==sid.toString()])
          return val.eq(sid)?val:acc
        },null)
        if (!s)
        {
          log.err('populating FAIL! sid not found!',sid)
          return
        }
        this._parent._populated[this._code][si] = s
      })
    })
  }

  populateFromConf(depth=0)
  {
    return Rx.Observable.create(obs=>{
      this._parent._populated[this._code] = []
      let tb = this.confOp(0,null)
      if (!tb)
        this.obsEnd(obs,this)
      let $srchs = []
      let nodes = {}
      let ids = this._parent._model[this._code].map(x=>{
        if (!x) return null
        if (x instanceof ObjectId) return x
        if (x._id) return x
        return null
      }).filter(x=>!!x)
      //console.log(['RELATION FIELD POPULATE',depth,this.code,ids])
      this.C(tb).findNodes({_id:{$in:ids}},{populate:depth}).subscribe(slist=>{
        this._parent._model[this._code].forEach((nid,ni)=>{
          this._parent._populated[this._code][ni] = slist.reduce((acc,x)=>{
            if (!x||!nid) return acc
            if (nid instanceof ObjectId && x.eq(nid)) return x
            if (nid._id && x.eq(nid._id)) return x
            return acc
          },null)
        })
        this.obsEnd(obs,this)
      },err=>{
        log.err('populate by config failed!',[this._code,targets])
        this.obsEnd(obs,this)
      })
    })
  }

  populate(depth=0)
  {
    return this.populateFromConf(depth)
  }

  xtypes()
  {
    let ret = ['relation']
    if (this._targetBundle&&this._targetBundle.length)
      ret.push('relation:'+this._targetBundle)
    if (this.quantity!=1)
      ret = ret.map(x=>'['+x+']')
    if (Array.isArray(this._xtypes)&&this._xtypes.length)
      this._xtypes.forEach(xt=>{
        if (!ret.includes(xt))
          ret.push(xt)
      })
    //console.log(this.code,ret,this._xtypes)
    return ret
  }
}

module.exports = Takatan.register(RelationField)
