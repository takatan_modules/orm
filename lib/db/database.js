const { Takatan, Log } = require('@takatanjs/core')
const Kefir = require('kefir')

const log = Log.m('ORM[AbstractDatabase]')
const Bundle = require('../bundle/bundle')

class AbstractDatabase extends Takatan.extends('AbstractCoreObject')
{

  static info()
  {
    return {
      code: 'abstractdb',
      name: 'Abstract DataBase',
      description: 'Abstract database interface',
      features: [], //json, jsonb, geodata, relation, key, bundle
      type: 'abstract' //table, document, graph, key
    }
  }

  constructor(core)
  {
    super(core)
    this._dbInfo = null
    this._connection = null
  }

  init(dbInfo)
  {
    this._dbInfo = dbInfo
  }

  connection()
  {
    return this._connection
  }

  bundleClass()
  {
    return Bundle
  }

  loadBundles()
  {
    return Kefir.constantError({code:'ormerror.database.should_be_reimplemented'})
  }

  saveBundle(code,bundle)
  {
    return Kefir.constantError({code:'ormerror.database.should_be_reimplemented'})
  }

  clearDB()
  {
    return Kefir.constantError({code:'ormerror.database.should_be_reimplemented'})
  }

  dropBundleTable(tbl,dropData=false)
  {
    return Kefir.constantError({code:'ormerror.database.should_be_reimplemented'})
  }

  connect(cfg)
  {
    return Kefir.constantError({code:'ormerror.database.should_be_reimplemented'})
  }

  disconnect()
  {
    return Kefir.constantError({code:'ormerror.database.should_be_reimplemented'})
  }

}

module.exports = Takatan.register(AbstractDatabase)
