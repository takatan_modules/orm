const { Takatan, Log } = require('@takatanjs/core')
const Kefir = require('kefir')

const log = Log.m('ORM[DB]')

const AbstractDatabase = require('./database')

Takatan.mix({
  B(...args)
  {
    if (this.M('bundle'))
      return this.M('bundle').bundle(...args)
    return null
  },
  C(code)
  {
    if (!this.M('bundle')||!this.M('bundle').collection(code))
      return null
    return this.M('bundle').collection(code)
  }
})


class ORMDbModule extends Takatan.extends('AbstractFactoryCoreModule')
{

  constructor(parent)
  {
    super(parent)

    this.factoryInit('db',AbstractDatabase)

    this._databases = {}
    this._defdb = null
  }

  defdb() { return this._defdb }

  getDbOfType(type)
  {
    for(let dbCode in this._databases)
      if (this._databases[dbCode].meta().type === type)
        return this._databases[dbCode]
    return null
  }

  /*
  bestDbFor(type)
  {
    for(let dbcode in this._databases)
    {
      console.log(this._databases[dbcode])
    }
  }
  */

  moduleConfigure(cfg)
  {
    return Kefir.stream(obs=>{
      if (!Array.isArray(cfg.databases)||!cfg.databases.length)
        return this.obsErr(obs,{code:'orm.no_db',error:'at least one database required!'})

      this._config = cfg
      let $t = []

      cfg.databases.forEach(dbi=>{
        let Db = this.factoryClass('db',dbi.type)
        this._databases[dbi.code] = new Db(this)
        this._databases[dbi.code].init(dbi)
        if (dbi.default||cfg.databases.length===1)
          this._defdb = this._databases[dbi.code]
        $t.push(this._databases[dbi.code].connect())
      })
      if (!this._defdb)
        return this.obsErr(obs,{code:'ormdb.no_default_db'})
      this.obsEnd(obs,$t)
    })
      .flatMap(x=>{
        return Kefir.concat(x)
      })
      .last()
    /**
    return Rx.of(_cfg).pipe(
      RxO.mergeMap(cfg=>{
        if (!Array.isArray(cfg.databases)||!cfg.databases.length)
          return Rx.throwError({code:'orm.no_db',error:'at least one database required!'})

        this._config = cfg
        let $t = []

        cfg.databases.forEach(dbi=>{
          let Db = this.factoryClass('db',dbi.type)
          this._databases[dbi.code] = new Db(this)
          this._databases[dbi.code].init(dbi)
          if (dbi.default||cfg.databases.length===1)
            this._defdb = this._databases[dbi.code]
          $t.push(this._databases[dbi.code].connect())
        })
        if (!this._defdb)
          return Rx.throwError({code:'ormdb.no_default_db'})
        return Rx.concat(...$t)
      }),
      RxO.last(),
      RxO.tap(_=>{
        log.success('configured and initialized')
      })
    )
    /**/
  }

    /**
  moduleStart()
  {
    return Rx.Observable.create(obs=>{
      let bndl = this.M('bundle').bundle('databases',true,'postgisbundle')
      console.log('bndl!',bndl)
      this.obsEnd(obs)
    }).pipe(
      RxO.tap(_=>log.success('started'))
    )
  }
    /**/

  moduleStop()
  {
    return Kefir.stream(obs=>{
        let $t = Object.keys(this._databases).map(dbcode=>this._databases[dbcode].disconnect())
        return $t
    })
      .flatMapConcat(x=>x)
      .last()
    /*
    return Rx.of(1).pipe(
      RxO.mergeMap(_=>{
        let $t = Object.keys(this._databases).map(dbcode=>this._databases[dbcode].disconnect())
        return Rx.concat(...$t)
      }),
      RxO.last(),
      RxO.tap(_=>{
        log.success('databases disconnected')
      })
    )
    */
  }

}

module.exports = Takatan.register(ORMDbModule)
