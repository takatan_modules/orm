//const Takatan = require('@takatan/core')
//const log = Takatan.log.m('ORM[AbstractFlaged]')
const { Takatan } = require('@takatanjs/core')

class AbstractFlaged
{

  /**
  @constructor
  */
  constructor()
  {
    this._flags = []
  }

  flags(val)
  {
    if (Array.isArray(val))
      this._flags = val
    return this._flags
  }

  hasFlag(key)
  {
    return Array.isArray(this._flags)&&this._flags.includes(key)
  }

  addFlag(key)
  {
    if (this.hasFlag(key)) return
    this._flags.push(key)
  }

  deleteFlag(key)
  {
    if (!this.hasFlag(key)) return
    this._flags = this._flags.filter(f=>f!=key)
  }


}

module.exports = Takatan.register(AbstractFlaged)
