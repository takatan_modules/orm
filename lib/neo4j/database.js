const Rx = require('rxjs')
const RxO = require('rxjs/operators')
const Takatan = require('@takatanjs/core')

const log = Takatan.log.m('ORM[Neo4j]')
const AbstractDatabase = require('../db/database')

class Neo4jDatabase extends AbstractDatabase
{

  static info()
  {
    return {
      code: 'neo4jdb',
      name: 'Neo4j DB',
      description: 'Neo4j database store graph type information',
      features: ['graph'],
      type: 'graph'
    }
  }

  connection()
  {
    return null
  }

  clearDB()
  {
    return Rx.throwError({code:'error.should_be_reimplemented'})
  }

  dropBundleTable(tbl,dropData=false)
  {
    return Rx.throwError({code:'error.should_be_reimplemented'})
  }

  connect(cfg)
  {
    return Rx.throwError({code:'error.should_be_reimplemented'})
  }

  disconnect(cfg)
  {
    return Rx.throwError({code:'error.should_be_reimplemented'})
  }

}

module.exports = Neo4jDatabase
