const Rx = require('rxjs')
const RxO = require('rxjs/operators')
const Takatan = require('@takatanjs/core')

const log = Takatan.log.m('ORM[Neo4jModule]')

const Neo4jDatabase = require('./database')

class ORMNeo4jModule extends Takatan.AbstractCoreModule
{

  moduleLoad()
  {
    return Rx.Observable.create(obs=>{
      this.M('db').factoryRegisterClass('db',Neo4jDatabase)
      this.obsEnd(obs)
    })
  }

}

module.exports = ORMNeo4jModule
