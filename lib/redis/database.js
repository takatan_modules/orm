const Rx = require('rxjs')
const RxO = require('rxjs/operators')
const Takatan = require('@takatanjs/core')

const log = Takatan.log.m('ORM[Redis]')
const AbstractDatabase = require('../db/database')

class RedisDatabase extends AbstractDatabase
{

  static info()
  {
    return {
      code: 'redisdb',
      name: 'Redis DB',
      description: 'Redis database store key->value type information',
      features: ['key'],
      type: 'key'
    }
  }

  connection()
  {
    return null
  }

  clearDB()
  {
    return Rx.throwError({code:'error.should_be_reimplemented'})
  }

  dropBundleTable(tbl,dropData=false)
  {
    return Rx.throwError({code:'error.should_be_reimplemented'})
  }

  connect(cfg)
  {
    return Rx.throwError({code:'error.should_be_reimplemented'})
  }

  disconnect(cfg)
  {
    return Rx.throwError({code:'error.should_be_reimplemented'})
  }

}

module.exports = RedisDatabase
