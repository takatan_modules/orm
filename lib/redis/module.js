const Rx = require('rxjs')
const RxO = require('rxjs/operators')
const Takatan = require('@takatanjs/core')

const log = Takatan.log.m('ORM[RedisModule]')

const RedisDatabase = require('./database')

class ORMRedisModule extends Takatan.AbstractCoreModule
{

  moduleLoad()
  {
    return Rx.Observable.create(obs=>{
      this.M('db').factoryRegisterClass('db',RedisDatabase)
      this.obsEnd(obs)
    })
  }

}

module.exports = ORMRedisModule
