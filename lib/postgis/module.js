const Rx = require('rxjs')
const RxO = require('rxjs/operators')
const Takatan = require('@takatanjs/core')

const log = Takatan.log.m('ORM[PostgisModule]')

const PostgisDatabase = require('./database')
const PostgisBundle = require('./bundle')

class ORMPostgisModule extends Takatan.AbstractCoreModule
{

  moduleRegister()
  {
    return Rx.Observable.create(obs=>{
      this.M('db').factoryRegisterClass('db',PostgisDatabase)
      this.M('bundle').factoryRegisterClass('bundle',PostgisBundle)
      this.obsEnd(obs)
    })
  }

}

module.exports = ORMPostgisModule
