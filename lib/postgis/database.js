const Rx = require('rxjs')
const RxO = require('rxjs/operators')
const Takatan = require('@takatanjs/core')

const log = Takatan.log.m('ORM[Postgis]')
const AbstractDatabase = require('../db/database')

const { Sequelize } = require('sequelize')

class PostgisDatabase extends AbstractDatabase
{

  static info()
  {
    return {
      code: 'postgis',
      name: 'Postgis DB',
      description: 'Postgis database store table type information',
      features: ['json','jsonb','geodata','relation'],
      type: 'table'
    }
  }

  connection()
  {
    return null
  }

  clearDB()
  {
    return Rx.throwError({code:'error.should_be_reimplemented'})
  }

  dropBundleTable(tbl,dropData=false)
  {
    return Rx.throwError({code:'error.should_be_reimplemented'})
  }

  connect()
  {
    return Rx.Observable.create(obs=>{
      let cfg = this._dbInfo.config
      cfg.dialect = 'postgres'
      cfg.logging = (msg,...o)=>log.debug(msg,o)
      log.test('connecting',cfg)
      this._connection = new Sequelize(cfg)
      this._connection.authenticate()
        .then(_=>{
          log.success('connected')
          this.obsEnd(obs)
        })
        .catch(err=>{
          log.error('connection failed',err)
          this.obsErr(obs,err)
        })
    })
  }

  disconnect()
  {
    return Rx.throwError({code:'error.should_be_reimplemented'})
  }

}

module.exports = PostgisDatabase
