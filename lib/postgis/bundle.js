const Rx = require('rxjs')
const RxO = require('rxjs/operators')
const Takatan = require('@takatanjs/core')

let log = Takatan.log.m('PostgisBundle')

const Bundle = require('../bundle/bundle')
//const Collection = require('./collection')
//const Node = require('../node/node')

class PostgisBundle extends Bundle
{

  static info()
  {
    return {
      code: 'postgisbundle',
      name: 'Postgis Bundle',
      description: 'Basic bundle for document type data'
    }
  }

  compile(dropData=false)
  {
    log.err('opopop')
    return Rx.throwError({code:'ormerror.bundle.should_be_reimplemented'})
  /**
    return Rx.Observable.create(obs=>{
      log.log('compiling',this._code)
      let schema = {}
      this._fields.forEach(f=>{
        let fschema = this.M('field').fieldSchema(f)
        if (this._fieldHasFlag(f,'index'))
          fschema.index = true
        if (this._fieldHasFlag(f,'required'))
        {
          fschema.index = true
          fschema.required = true
        }
        if (typeof f.def != 'undefined')
          fschema.default = f.def
        schema[f.code] = [fschema]
      })
      this.M('db').dropBundleTable(this.dbTable(),dropData).subscribe(()=>{
        let clsNode = null
        let clsFn = null
        if (this._collection)
        {
          clsNode = this._collection._nodeClass || null
          clsFn = this._collection._classFn || null
        }

        let collectionClass = this._core.cls('collection')
        this._collection = new collectionClass(this,schema)
        if (clsNode)
          this._collection.setNodeClass(clsNode)
        if (clsFn)
          this._collection.setNodeClassFn(clsFn)
        log.log('compiled',this._code)
        this.onCompiled.next(this)
        let next = Rx.of(1)
        if (dropData)
          next = this._collection.removeNodes({})
        next.subscribe(()=>{
          obs.next()
          obs.complete()
        },err=>{obs.error(err)})
      })
    })
  /**/
  }

  drop()
  {
  /**
    return this.M('db').dropBundleTable(this.dbTable(),true).pipe(
      RxO.mergeMap(_=>{
        return Rx.from(MgBundle.findOneAndRemove({code:this._code}))
      })
    )
  /**/
  }

}

module.exports = PostgisBundle
