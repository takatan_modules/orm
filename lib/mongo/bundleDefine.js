let crypto = require('crypto')

mongoose = require('mongoose')

var BundleSchema = mongoose.Schema({
  type: {
    type: String,
    required: true
  },
  prefix: {
    type: String,
    required: true
  },
  name: {
    type: String,
    required: true
  },
  code: {
    type: String,
    required: true,
    unique: true,
    index: true
  },
  description: {
    type: String,
    required: false
  },
  flags: {
    type: [String],
    default: []
  },
  /* ======= */
  fields: [{
    /* BUNDLE FIELD SCHEMA */
    name: {
      type: String,
      required: true
    },
    code: {
      type: String,
      required: true
    },
    description: {
      type: String,
      required: false,
    },
    type: {
      type: String,
      required: true
    },
    flags: {
      type: [String],
      default: []
    },
    quantity: {
      type: Number,
      default: 1
    },
    generator: {
      type: mongoose.Schema.Types.Mixed,
      default: null
    },
    defaultValue: {
      type: mongoose.Schema.Types.Mixed
    },
    config: {
      type: mongoose.Schema.Types.Mixed,
      default: null
    }
   /* ======= */
  }],
  relations: [{
    /* RELATION DESCRIPTION SCHEMA */
    type: {
      type: String,
      required: true
    },
    config: {
      type: mongoose.Schema.Types.Mixed,
      default: null
    }
  }]
  /**/
})

module.exports = mongoose.model('MgBundle',BundleSchema)
