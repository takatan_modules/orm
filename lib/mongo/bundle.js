//const Rx = require('rxjs')
//const RxO = require('rxjs/operators')
//const Takatan = require('@takatanjs/core')
const { Takatan, Log } = require('@takatanjs/core')
const Kefir = require('kefir')

const log = Log.m('MongoBundle')

const Bundle = require('../bundle/bundle')
const Collection = require('./collection')
//const Node = require('../node/node')

class MongoBundle extends Takatan.extends(Bundle)
{

  constructor(...args)
  {
    super(...args)
    this.db(this.M('db').getDbOfType('mongodb'))
    //console.log(this._db)
  }

  static info()
  {
    return {
      code: 'mongobundle',
      name: 'Mongo Bundle',
      description: 'Basic bundle for document type data'
    }
  }

  compile(dropData=false)
  {
    //return Rx.throwError({code:'ormerror.bundle.should_be_reimplemented'})
  /**/
    return this.db().dropBundleTable(this.dbTable(),dropData)
      .flatMap(_=>{
        log.log('compiling',this._code)
        let schema = {}
        this._fields.forEach(f=>{
          let fschema = this.M('field').fieldSchema(f,{},'mongodb')
          /*
          if (this._fieldHasFlag(f,'index'))
            fschema.index = true
          if (this._fieldHasFlag(f,'required'))
          {
            fschema.index = true
            fschema.required = true
          }
          */
          if (typeof f.defaultValue != 'undefined')
            fschema.default = f.defaultValue
          schema[f.code] = [fschema]
        })
        //console.log('schema',schema)
        let clsNode = null
        let clsFn = null
        if (this._collection)
        {
          clsNode = this._collection._nodeClass || null
          clsFn = this._collection._classFn || null
        }

        let collectionClass = Collection
        this._collection = new collectionClass(this,schema)
        if (clsNode)
          this._collection.setNodeClass(clsNode)
        if (clsFn)
          this._collection.setNodeClassFn(clsFn)
        log.success('compiled',this._code)
        this.onCompiled.plug(Kefir.constant(this))
        let next = Kefir.constant(1)
        if (dropData)
          next = this._collection.removeNodes({})
        return next
          .map(_=>{
            return this._collection
          })
      })
  /**/
  }

  drop()
  {
    return this.db().dropBundleTable(this.dbTable(),dropData)
      .flatMap(_=>{
        return Kefir.fromPromise(MgBundle.findOneAndRemove({code:this._code}))
      })
  }

}

module.exports = Takatan.register(MongoBundle)
