const { Takatan, Log } = require('@takatanjs/core')
const Kefir = require('kefir')

const log = Log.m('ORM[MongoModule]')

const MongoDatabase = require('./database')
const DocumentBundle = require('./bundle')

class ORMMongoModule extends Takatan.extends('AbstractCoreModule')
{

  moduleRegister()
  {
    return Kefir.stream(obs=>{
      this.M('db').factoryRegisterClass('db',MongoDatabase)
      this.M('bundle').factoryRegisterClass('bundle',DocumentBundle)
      this.obsEnd(obs)
    })
  }

  moduleInstall()
  {
    return Kefir.stream(obs=>{
      this.obsEnd(obs,{
        uri: 'mongodb://127.0.0.1:27017/defdb'
      })
    })
  }

}

module.exports = Takatan.register(ORMMongoModule)
