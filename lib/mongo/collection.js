const { Takatan, Log } = require('@takatanjs/core')
const Kefir = require('kefir')
const mg = require('mongoose')

const log = Log.m('MongoCollection')

const AbstractCollection = require('../bundle/collection')

class MongoCollection extends Takatan.extends(AbstractCollection)
{

  _initModel()
  {
    this._model = mg.model(this.bundle().dbTable(),this._schema)
  }


  _mg2mdl(mg)
  {
    if (!mg)
      return mg
    if (typeof mg.toObject != 'function')
    {
      //log.error('_mg2mdl mg.toObject is not a function!')
      //console.log(mg)
      return mg
    }
    //log.warn('_mg2mdl mg.toObject is a function!')
    let mdl = mg.toObject()
    return mdl
  }

  saveNode(mdl)
  {
    return Kefir.stream(obs=>{
      if (mdl&&mdl.__v)
        delete mdl.__v
      let nx = null
      if (mdl._id)
        nx = Kefir.fromPromise(this._model.findOneAndUpdate({_id:mdl._id},mdl,{upsert:true,new:true}))
      else
        nx = Kefir.fromPromise(this._model.create(mdl))
      nx.observe(smdl=>{
        let ret = this._mg2mdl(smdl)
        log.log('SAVED NODE!',ret)
        this.obsEnd(obs,ret)
      },err=>{
        this.obsErr(obs,err)
      })
    })
  }

  loadNode(mdl,opts=null)
  {
    let m = this._mg2mdl(mdl)
    log.log('load node',m)
    let Cls = this.nodeClass(m,opts)
    let node = new Cls(this,m)
    return node
  }

  newNode(defs=null)
  {
    let m = new this._model()
    let mdl = this._mg2mdl(m)
    log.log('new node',mdl)
    if (defs && typeof defs == 'object')
      try
      {
        for(let key in defs)
          mdl[key] = defs[key]
      } catch(e) {
      }
    let Cls = this.nodeClass(mdl)
    let node = new Cls(this,mdl,true)
    return node
  }

  initNode(ctx=null)
  {
    /*
    return Rx.Observable.create(obs=>{
      let node = this.newNode()
      log.log('initializing new node',node.bundle())
      node._initDefaults(ctx).subscribe(()=>{
        node.init(ctx).subscribe(()=>{
          obs.next(node)
          obs.complete()
        },err=>{
          console.log(['init error',err])
          obs.error(err)
        })
      },err=>{
        console.log(['_initDefault error',err])
        obs.error(err)
      })
    })
    */
  }

  findId(id)
  {
    return this.findNode({_id:id})
  }

  findNode(query,opts)
  {
    return Kefir.stream(obs=>{
      let nxOpts = opts
      if (!nxOpts)
        nxOpts = {limit:1}
      else
        nxOpts.limit = 1
      this.findNodes(query,nxOpts).observe(listOne=>{
        let ret = null
        if (listOne[0])
          ret = listOne[0]
        this.obsEnd(obs,ret)
      },err=>this.obsErr(obs,err))
    })
  }

  findNodes(query,opts)
  {
    return Kefir.stream(obs=>{
      if (!this._model)
        return this.obsErr(obs,{code:'collection',collection:null,error:'COLLECTION_MODEL_NOT_EXISTS'})

      let listMode = true //was false default
      if (typeof opts == 'boolean'&&opts)
        opts = { populate: 1 }
      if (typeof opts == 'object')
      {
        if (typeof opts.list!='undefined')
          listMode = opts.list
      }

      let populateDepth = 0
      if (opts&&opts.populate&&typeof opts.populate == 'number')
        populateDepth = opts.populate

      let sort = null
      let limit = null
      let skip = null
      if (opts)
      {
        if (opts.sort)
          sort = opts.sort
        if (opts.limit)
          limit = opts.limit
        if (opts.start)
          skip = opts.start
        if (opts.skip)
          skip = opts.skip
      }

      let q = this._model.find(query)
      if (sort) q = q.sort(sort)
      if (limit) q = q.limit(limit)
      if (skip) q = q.skip(skip)

      let pops = []
      if (populateDepth>0)
        pops = this._buildPopulateGraph(this.bundle().code(),populateDepth-1)
      pops.forEach(p=>{
        q = q.populate(p)
      })
      Kefir.fromPromise(q.exec()).observe(list=>{
        let $nodes = []
        if (Array.isArray(list))
        {
          list.forEach(el=>{
            let nd = this.loadNode(el,opts)
            $nodes.push(nd.init())
          })
        }
        let nodes = []
        Kefir.concat($nodes)
          .takeErrors(1)
          .observe(n=>nodes.push(n),err=>obs.error(err),()=>{
            if (listMode)
              return this.obsEnd(obs,nodes)
            nodes.forEach(nd=>{
              obs.emit(nd)
            })
            obs.end()
          })
      },err=>{
        log.error('collection search error!')
        console.log(err)
        this.obsErr(obs,err)
      })
    })
  }

  _buildPopulateGraph(targetBundle,depth=0)
  {
    let ret = []
    /**/
    let tb = this.M('bundle').bundle(targetBundle)
    tb.fields().forEach(fi=>{
      let fieldTarget = this.M('field').fieldPopulateSchema(fi)
      //console.log(['collection buildPopulateGraph','target',fieldTarget])
      if (!fieldTarget||!fieldTarget.length)
        return
      let cll = this.C(fieldTarget)._model
      let nx = {
        path: fi.code,
        model: cll
      }
      if (depth>0)
        nx.populate = this._buildPopulateGraph(fieldTarget,depth-1)
      ret.push(nx)
    })
    //console.log([ret,'graph for',targetBundle,depth])
    /**/
    return ret
  }

  _oidEq(oid1,oid2)
  {
    if (!oid1||!oid2) return false
    if (oid1==oid2) return true
    if (typeof oid1.toString == 'function' && typeof oid2.toString == 'function' && oid1.toString()==oid2.toString()) return true
    if (oid1+''!='[object Object]' && oid2+''!='[object Object]' && oid1+''==oid2+'' ) return true
    return false
  }

  updateNode(query,data,params)
  {
    return Kefir.stream(obs=>{
      log.warn('CHECK HOW ::updateNode IS WORKING')
      if (!this._model)
      {
        log.error('collection model non exists')
        return this.obsErr(obs,{code:'collection',collection:null,error:'COLLECTION_MODEL_NOT_EXISTS'})
      }
      if (!this._nodeClass)
        this._nodeClass = this._core.cls('node')
      this._model.findOneAndUpdate(query,data,params,(err,doc)=>{
        let node = null
        if (err||!doc)
          return this.obsEnd(obs,node)

        let Cls = this._nodeClass
        if (this._classFn)
        {
          let CClsfn = this._classFn(el)
          if (Cls.isPrototypeOf(CClsfn))
            Cls = CClsfn
        }
        node = new Cls(this,doc)
        this.obsEnd(obs,node)
      })
    })
  }

  countNodes(query)
  {
    return Kefir.stream(obs=>{
      if (!this._model)
      {
        log.error('collection model non exists')
        return this.obsErr(obs,{code:'collection',collection:null,error:'COLLECTION_MODEL_NOT_EXISTS'})
      }
      this._model.countDocuments(query,(err,count)=>{
        if (err)
        {
          log.error('collection count fails',err)
          this.obsErr(obs,{code:'collection',collection:this._bundle.code,error:'COLLECTION_COUNT_FAILS'})
          return
        }
        this.obsEnd(obs,count)
      })
    })
  }

  removeNodes(query)
  {
    return Kefir.stream(obs=>{
      Kefir.fromPromise(this._model.find(query)).observe(list=>{
        let ids = list.map(x=>x._id)
        //this._core.hookMap('nodesBeforeRemove',{bundle:this.code(),nids:ids}).subscribe(res=>{
          //if (!res||!res.nids||!res.nids.length)
            //return this.obsEnd(obs,res)
          this._model.deleteMany({_id:{$in:ids}},(err,dres)=>{
            if (err)
              return this.obsErr(obs,err)
            //res.bundle = this.code()
            //this._core.hookMap('nodesAfterRemove',res).subscribe(()=>{
              //this.obsEnd(obs,res)
            //},err=>{
              //log.error('nodeAfterDelete hook error!')
              //console.log(err)
              this.obsEnd(obs,dres)
            //})
          })
        //},err=>{
          //log.error('nodeBeforeDelete hook error!')
          //console.log(err)
          //this.obsErr(obs,err)
        //})
      },err=>{
        log.error('removeNode prepare id for hooks error!')
        console.log(err)
        this.obsErr(obs,err)
      })
    })
  }

  /*
  regenerateFields(codes)
  {
    return Rx.Observable.create(obs=>{
      log.info('regenrating field',codes)
      this.findNodes({}).subscribe(list=>{
        let $t = []
        list.forEach(node=>{
          codes.forEach(code=>{
            if (!node.field(code)) return
            $t.push(node.field(code).genDefault())
          })
          $t.push(node.save())
        })
        Rx.concat(...$t).subscribe(el=>{
          log.info('..')
        },err=>{
          log.error('findNodes error')
          console.log(err)
          this.obsEnd(obs,{success:false,error:'Find nodes fail'})
        },()=>{
          log.success('fields regenerated',codes)
          this.obsEnd(obs,{success:true})
        })
      },err=>{
        log.error('findNodes error')
        console.log(err)
        this.obsEnd(obs,{success:false,error:'Find nodes fail'})
      })
    })
  }
  /**/

}

module.exports = MongoCollection
