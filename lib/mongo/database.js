const { Takatan, Log } = require('@takatanjs/core')
const Kefir = require('kefir')

const log = Log.m('ORM[Mongo]')
const AbstractDatabase = require('../db/database')
const MongoBundle = require('./bundle')

const mg = require('mongoose')
mg.set('useCreateIndex',true)
mg.set('useFindAndModify',false)
mg.Promise = global.Promise

const MgBundle = require('./bundleDefine')

class MongoDatabase extends Takatan.extends(AbstractDatabase)
{

  static info()
  {
    return {
      code: 'mongodb',
      name: 'Mongo DB',
      description: 'Mongo database store document type information',
      features: ['key'],
      type: 'key'
    }
  }

  bundleClass()
  {
    return MongoBundle
  }

  loadBundles()
  {
    return Kefir.fromPromise(MgBundle.find({}))
  }

  saveBundle(code,bundle)
  {
    return Kefir.fromPromise(MgBundle.findOneAndUpdate({code:code},bundle,{upsert:true,new: true}))
    //return Rx.throwError({code:'ormerror.database.should_be_reimplemented'})
  }

  clearDB()
  {
    return Kefir.stream(obs=>{
      log.warn('clearing db!')
      this._connection.db.dropDatabase(()=>{
        for(let m in this._connection.models)
          if (m[1]=='_')
            delete this._connection.models[m]
        this.obsEnd(obs)
      })
    })
  }

  dropBundleTable(tbl,dropData=false)
  {
    return Kefir.stream(obs=>{
      log.log('dropBundleTable',{table:tbl,withData:dropData})
      if (!this._connection||!this._connection.models[tbl])
        return this.obsEnd(obs)
      if (dropData)
      {
        log.warn('dropBundleTable with data',tbl)
        //console.log(this._connection.models[tbl])
        this._connection.db.dropCollection(tbl,(err,res)=>{
          this._connection.models[tbl].collection.drop((err,res)=>{
            log.warn('data dropped!',tbl)
            delete this._connection.models[tbl]
            this.obsEnd(obs)
          })
        })
      } else {
        delete this._connection.models[tbl]
        this.obsEnd(obs)
      }
    })
  }

  connect()
  {
    return Kefir.stream(obs=>{
      let cfg = this._dbInfo.config
      let url = `mongodb://${cfg.host}/${cfg.database}`

      log.test('connecting to database',url)
      this._connection = mg.connection
      this._connection.once('error',err=>{
        log.error('connection failed!',err)
        this.obsErr(obs,err)
      })
      this._connection.once('open',_=>{
        log.success('connected!')
        this.obsEnd(obs)
      })
      mg.connect(url,{
        //user: cfg.username,
        //password: `${cfg.password}`,
        useNewUrlParser:true,
        useUnifiedTopology: true
        //family: 6
      })
    })
  }

  disconnect()
  {
    return Kefir.constantError({code:'error.should_be_reimplemented'})
  }

}

module.exports = Takatan.register(MongoDatabase)
