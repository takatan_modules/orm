const { Takatan, Log } = require('@takatanjs/core')
const Kefir = require('kefir')

const log = Log.m('ORM[Node]')

const Node = require('./node')

class ORMNodeModule extends Takatan.extends('AbstractFactoryCoreModule')
{

}

module.exports = Takatan.register(ORMNodeModule)
