const { Takatan, Log } = require('@takatanjs/core')
const Kefir = require('kefir')

let log = Log.m('VirtualNode')

//const CoreObject = require('../core/abstract/object')
const Node = require('./node')

class VirtualNode extends Takatan.extends(Node)
{

  constructor(parent,model={},isNew=false)
  {
    super(parent,model,true)

    this._type = 'vnode'
    this._doHooks = false

    this._model = model
    this._model.markModified = (code)=>{}
    this._fields = {}
    this._populated = {}
  }

  _initFields()
  {
  }

  clearFields()
  {
    this._model = {}
    this._model.markModified = (code)=>{}
    this._fields = {}
    this._populated = {}
  }
  /*
  clone()
  {
    let Cls = this.constructor
    let mdl = JSON.parse(JSON.stringify(this._model))
    if (typeof mdl._id != 'undefined')
      delete mdl._id
    let ret = new Cls(this._parent,mdl,true)
    ret._populated = this._populated
    return ret
  }
  /**/

  eq(node)
  {
    return false
  }

  id()
  {
    return ''
  }

  bundle()
  {
    return 'virtual'
  }

  bundleFields()
  {
    let ret = []
    for(let fcode in this._fields)
      ret.push(this._fields[fcode]._info)
    //console.log(['vnodeBundleFields()',ret])
    return ret
  }

  save()
  {
    return Kefir.constant(this)
  }

  //virtual as real fields
  addField(info,overwrite=true)
  {
    if (this._fields[info.code]&&!overwrite)
      return
    if (!Array.isArray(info.flags))
      info.flags = []
    if (!info.flags.includes('virtual'))
      info.flags.push('virtual')
    if (!info.flags.includes('logic'))
      info.flags.push('logic')
    if (!info.flags.includes('template'))
      info.flags.push('template')
    if (typeof info.quantity == 'undefined')
      info.quantity = 1
    let f = this.M('field').genField(this,info)
    //if (info.code=='senderUser')
    //console.log(['adField',info])
    this._fields[info.code] = f
    this._model[info.code] = []
    //this._model[info.code].markModified = (path)=>{}
    this._populated[info.code] = []
  }

}

module.exports = Takatan.register(VirtualNode)
