const { Takatan, Log } = require('@takatanjs/core')
const Kefir = require('kefir')

const log = Log.m('Node')

class Node extends Takatan.extends('AbstractCoreObject')
{

  constructor(parent,model,op1)
  {
    super(parent)
    this._model = model
    //console.log('new node',parent,model,op1)
    this._parent = parent
    this._collection = parent
    this._core = parent._core
    this._bundle = this._parent._bundle

    this._type = 'node'
    this._doHooks = true

    this._new = false
    this._saved = !this._new
    this._fields = {}
    this._pops = null
    this._populated = {}
    if (typeof op1 == 'boolean')
      this._new = op1
    if (typeof op1 == 'object')
      this._pops = op1
    this._initFields()
  }

  _initFields()
  {
    //console.log(['Node initFields',this._model])
    this.bundleFields().forEach(info=>{
      let f = this.M('field').genField(this,info)
      this._fields[f.code()] = f
      this._populated[f.code()] = []
      f.init()
    })
    //console.log(['Node initFields done',this._model,this._populated])
  }

  /*
  dashboardTitle(withBundle=false)
  {
    let ret = ''
    this.bundleFields().forEach(info=>{
      if (!Array.isArray(info.flags)||!info.flags.includes('title'))
        return
      let v = this.f(info.code)
      let vv = ''
      if (!v)
        return
      if (typeof v.toString == 'function')
      {
        vv = v.toString()
      } else {
        try { vv = v+'' } catch(ee) {}
      }
      if (!vv||!vv.length)
        return
      if (ret.length)
        ret += ' '
      ret += vv
    })
    if (!ret||!ret.length)
      ret = this.id()+''
    if (withBundle)
      ret += ' ['+this.bundle()+']'
    return ret
  }
  */

  init(ctx=null)
  {
    return Kefir.constant(this)
  }

  /*
  _initDefaults(ctx=null)
  {
    return Rx.Observable.create(obs=>{
      let $t = []
      for(let fcode in this._fields)
      {
        log.log('preparing generating default value for',fcode)
        $t.push(this._fields[fcode].genDefault(ctx))
      }
      Rx.concat(...$t).subscribe(n=>{},err=>{
        log.err('initing default failed')
        console.log(err)
        obs.error({code:'field',field:this.code,error:err})
      },()=>{
        obs.next()
        obs.complete()
      })
    })

  }

  /*
  clone()
  {
    let Cls = this.constructor
    let mdl = JSON.parse(JSON.stringify(this._model))
    if (typeof mdl._id != 'undefined')
      delete mdl._id
    let ret = new Cls(this._parent,mdl,true)
    ret._populated = this._populated
    return ret
  }
  /**/

  eq(node)
  {
    if (!node) return false
    let myid = this.id().toString()
    //if mongo _id 
    if (node.toString() === myid) return true
    if(!(node instanceof Node)) return false
    return myid == node.id().toString()
  }

  id()
  {
    //console.log(this)
    return this._model._id
  }

  bundle()
  {
    //if (!this._bundle) return 'virtual'
    return this._bundle.code()
  }

  bundleFields()
  {
    //if (!this._bundle) return []
    return this._bundle.fields()
  }

  /*
  type()
  {
    return this._type
  }
  */

  field(fcode)
  {
    if (!this._fields[fcode]) return null
    return this._fields[fcode]
  }

  f(code,val)
  {
    if (!this._fields[code]) return
    if (typeof val != 'undefined')
    {
      this.field(code).set(val)
    }
    return this.field(code).get()
  }

  fp(code,val)
  {
    if (!this._fields[code]) return
    if (typeof val != 'undefined')
    {
      this.field(code).set(val)
    }
    if (typeof this._populated[code][0] != 'undefined')
      return this._populated[code][0]
    return this.field(code).get()
  }

  _beforeSave()
  {
    return this.beforeSave()//.pipe(RxOps.mergeMap(x=>this._core.hookMap('nodeBeforeSave',x)))
  }

  _afterSave()
  {
    return this.afterSave()//.pipe(RxOps.mergeMap(x=>this._core.hookMap('nodeAfterSave',x)))
  }


  beforeSave()
  {
    return Kefir.constant(this)
  }

  afterSave()
  {
    return Kefir.constant(this)
  }
  /*
  */

  save()
  {
    return Kefir.stream(obs=>{
      let $t = []
      let failed = []
      for(let fcode in this._fields)
        $t.push(this._fields[fcode].validate())
      Kefir.concat($t).observe(next=>{
        if (!next.valid)
          failed.push(next)
      },err=>{
        obs.error({code:'valid',valid:false,errors:failed})
      },()=>{
        if (failed.length)
        {
          console.log(['node save validation error',failed,this._model])
          obs.error({valid:false,errors:failed,code:'valid'})
          return
        }
        let nextBeforeSave = this._beforeSave()
        //if (!this._doHooks)
          //nextBeforeSave = Rx.of(this)
        nextBeforeSave.observe(me=>{
          if (!me)
            return this.obsErr(obs,{code:'beforeSave',error:'IS_NULL',valid:false})
          //Rx.from(this._model.save()).subscribe(upd=>{
          this._parent.saveNode(this._model).observe(upd=>{
            if (!upd)
              return this.obsErr(obs,{code:'save',error:'IS_NULL',valid:false})
            this._model = upd
            this._saved = true
            let nextAfterSave = this._afterSave()
            //if (!this._doHooks)
              //nextAfterSave = Rx.of(this)
            nextAfterSave.observe(()=>{
              this._new = false
              this.obsEnd(obs,this)
            },err=>{
              log.error('Node afterSave FAIL',err)
              console.log(err)
              this._new = false
              this.obsErr(obs,{code:'afterSave',errors:err})
            })
          },err=>this.obsErr(obs,{code:'save',errors:err}))
        },err=>{
          this.obsErr(obs,{code:'beforeSave',errors:err})
        })
      })
    })
  }
  /*

  logicScope(nx)
  {
    if (typeof nx=='string'&&nx!='')
      this._logicScope = nx
    if (this._logicScope&&this._logicScope!='')
      return this._logicScope
    return this.bundle()
  }

  logicFields(lvl=5)
  {
    let ret = []
    if (lvl<0)
      return ret
    ret.push({
      scope: this.logicScope(),
      info: {flags:['virtual','readonly'],quantity:1,name:g18n.t('Node ID'),code:'id',type:'string',xtypes:['string:small']},
      values: [this.id().toString()]
    })
    ret.push({
      scope: this.logicScope(),
      info: {flags:['virtual','readonly'],quantity:1,name:g18n.t('Bundle'),code:'bundle',type:'string',xtype:['string:small']},
      values: [this.bundle()]
    })
    this.bundleFields().forEach(info=>{
      if (!this.field(info.code))
        return
      if (!this.field(info.code).hasFlag('logic'))
        return
      let vals = []
      let subs = []
      let dd = false
      this.field(info.code).all().forEach((a,i)=>{
        let val = this.field(info.code).getp(i)
        if (val instanceof Node)
          subs.push(val.logicFields(lvl-1))
        else
          vals.push(val)
      })
      //if (info.code == 'author' || info.code == 'coins')
        //console.log(['logicFields',this,info.code,vals])
      info.xtypes = this.field(info.code).xtypes()
      ret.push({
        scope: this.logicScope(),
        info: info,
        values: vals,
        subs: subs
      })
    })
    return ret
  }

  populate(depth=0)
  {
    return Rx.Observable.create(obs=>{
      let $t = []
      //console.log(['node populate',this._type,depth])
      this.bundleFields().forEach(info=>{
        $t.push(this.field(info.code).populate(depth))
      })
      Rx.concat(...$t).subscribe(()=>{},err=>{
        log.err('populate failed')
        console.log(err)
        this.obsEnd(obs,this)
      },()=>{
        this.obsEnd(obs,this)
      })
    })
  }


  toObject()
  {
    let obj = this.view(true)
    obj.bundle = this.bundle()
    return obj
  }
  /**/

  toObject()
  {
    let ret = {}
    this.bundleFields().forEach(info=>{
      if (typeof info.quantity !== 'number')
        info.quantity = 1
      if (info.quantity===1)
        ret[info.code] = this.f(info.code)
      else
        ret[info.code] = this.field(info.code).all()
    })
    ret.id = this.id()
    return ret
  }
}

module.exports = Takatan.register(Node)
